/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

#include "MuCTPIByteStreamTool.h"
#include "TrigT1Result/MuCTPI_RDO.h"
#include "TrigT1MuctpiBits/MuCTPI_Bits.h"
#include "TrigT1MuctpiBits/HelpersPhase1.h"

//Rafal's word decoding code from:
//home/ak/workspace/ctpfs/athenaDev/athena/Trigger/TrigT1/TrigT1ResultByteStream/src/MuonRoIByteStreamTool.cxx
//is reused below

// Unique interface ID of the tool that identifies it to the framweork
static const InterfaceID IID_IMuCTPIByteStreamTool( "MuCTPIByteStreamTool", 1, 1 );

/**
 * This function is needed by the framework to know what kind of tool
 * this is.
 */
const InterfaceID & MuCTPIByteStreamTool::interfaceID() {
  return IID_IMuCTPIByteStreamTool;
}

/**
 * The constructor takes care of correctly constructing the base class and
 * declaring the tool's interface to the framework.
 */
MuCTPIByteStreamTool::MuCTPIByteStreamTool( const std::string& type, const std::string& name,
                                            const IInterface* parent )
    : AthAlgTool( type, name, parent ) {

  declareInterface<MuCTPIByteStreamTool>( this );


}

/**
 * Conversion from RDO to eformat::ROBFragment.
 * This is called from the MuCTPIByteStreamCnv::createRep method.
 */
StatusCode MuCTPIByteStreamTool::convert( const MuCTPI_RDO* result, RawEventWrite* re ) {

  ATH_MSG_DEBUG("executing convert() from RDO to ROBFragment");

  // Clear Event Assembler
  m_fea.clear();

  // MIROD
  FullEventAssembler< MuCTPISrcIdMap >::RODDATA* theROD;

  // Source ID of MIROD
  const uint32_t rodId = m_srcIdMap.getRodID();

  // get the ROD data container to be filled
  theROD = m_fea.getRodData( rodId );

  ATH_MSG_VERBOSE(" Dumping MuCTPI words:");

  // fill Candidate Multiplicity
  const std::vector< uint32_t >& multiWord = result->getAllCandidateMultiplicities();
  std::vector< uint32_t >::const_iterator it   = multiWord.begin();
  std::vector< uint32_t >::const_iterator it_e = multiWord.end();
  for( ; it != it_e; ++it ) {
    theROD->push_back( *it );
    ATH_MSG_VERBOSE("     0x" << MSG::hex << std::setfill( '0' )
        << std::setw( 8 )  << ( *it ) << " (candidate multiplicity)");
  }
  
  // fill Data Words
  const std::vector< uint32_t >& dataWord = result->dataWord();
  it   = dataWord.begin();
  it_e = dataWord.end();
  for( ; it != it_e; ++it ) {
    theROD->push_back( *it );
    ATH_MSG_VERBOSE("     0x" << MSG::hex << std::setfill( '0' )
        << std::setw( 8 )  << ( *it ) << " (candidate word)");
  }

  // Now fill full event
  ATH_MSG_DEBUG("Now filling the event with the MuCTPI fragment");
  m_fea.fill( re, msg() );

  return StatusCode::SUCCESS;
}

/**
 * Conversion from eformat::ROBFragment to RDO.
 * This is called from the MuCTPIByteStreamCnv::createObj method.
 */
StatusCode MuCTPIByteStreamTool::convert( const ROBF* rob, MuCTPI_RDO*& result ) {

  ATH_MSG_DEBUG("executing convert() from ROBFragment to RDO");

  // Source ID of MIROD
  const uint32_t miRodId = m_srcIdMap.getRodID();

  // check ROD source ID
  const uint32_t rodId = rob->rod_source_id();

  // check BC ID
  const uint32_t bcId = rob->rod_bc_id();

  ATH_MSG_DEBUG(" expected ROD sub-detector ID: " << std::hex << miRodId 
      << " ID found: " << std::hex << rodId << std::dec);  

  if( rodId == miRodId || rodId == 0x7501 ) {

    ATH_MSG_VERBOSE(" ROD Header BCID " << bcId << ", dumping MuCTPI words:");
    if( rodId == 0x7501 ) {
      ATH_MSG_DEBUG(" Deprecated ROD source id found: " 
          << std::hex << rodId << std::dec);
    }

    // For generality let's declare the data pointer like this. Altough it's
    // unlikely to ever change from being a pointer to uint32_t-s.
    OFFLINE_FRAGMENTS_NAMESPACE::PointerType it_data;
    rob->rod_data( it_data );
    const uint32_t ndata = rob->rod_ndata();
    ATH_MSG_VERBOSE("MUCTPI DQ DEBUG: number of data words: " << ndata);
    ATH_MSG_WARNING("MUCTPI DQ DEBUG: number of ROB data words: " << std::dec << ndata);
    uint smk=0;
    bool success=false;
    TrigConf::L1CTPFiles ctpfiles;
    SG::ReadHandleKey<TrigConf::L1Menu>  L1MenuKey{this, "L1TriggerMenu", "DetectorStore+L1TriggerMenu", "L1 Menu"};

    //must be a better way to have this done only once...todo: check
    if(false)
    if(!m_enteredMuctpiNbits)
    {
        m_enteredMuctpiNbits=true;
        //todo: move this to the constructor perhaps? => CANNOT! no compile

        //fetch nbits vector from l1menu (todo: not do every time?)

        ATH_CHECK(L1MenuKey.initialize());

        SG::ReadHandle<TrigConf::L1Menu> l1Menu = SG::makeHandle(L1MenuKey);

        if (!l1Menu.isValid())
        {
            ATH_MSG_ERROR("MUCTPI DQ DEBUG: CTPFILES L1 menu INvalid! Cannot read nbits for mlt word");
        }
        else
        {
            ATH_MSG_WARNING("MUCTPI DQ DEBUG: CTPFILES L1 menu valid");
            smk = l1Menu->smk();
            //m_alias_db: should it be hardcoded? todo: check
            TrigConf::TrigDBCTPFilesLoader db_loader(m_alias_db);
            //options below added according to
            //TrigConf::L1CTPFiles header file
            ATH_MSG_WARNING("MUCTPI DQ DEBUG: CTPFILES L1 menu load files with smk="<<smk);
            try{success = db_loader.loadHardwareFiles(smk, ctpfiles,0x08);}
            catch(std::exception& e){ ATH_MSG_ERROR("MUCTPI DQ DEBUG: CTPFILES loadHardwareFiles exception: "<<e.what()); }
            catch(...){ ATH_MSG_ERROR("MUCTPI DQ DEBUG: CTPFILES loadHardwareFiles: Unknown exception"); }

            ATH_MSG_WARNING("MUCTPI DQ DEBUG: CTPFILES success="<<success);
        }
        if(!ctpfiles.hasCompleteMuctpiData())
        {
            ATH_MSG_ERROR("MUCTPI DQ DEBUG: CTPFILES Incomplete MUCTPI data from TriggerDB");
        }
        else if(success)
        {
            ATH_MSG_WARNING("MUCTPI DQ DEBUG: CTPFILES L1 menu load files success!");
            try
            {
                m_muctpi_Nbits = (std::vector<uint32_t>) ctpfiles.muctpi_Nbits();
                if(m_muctpi_Nbits.size()!=32)
                    ATH_MSG_ERROR("MUCTPI DQ DEBUG: CTPFILES MUCTPI data from TriggerDB - nbits wrong size: "<<std::dec << m_muctpi_Nbits.size());

                ATH_MSG_WARNING("MUCTPI DQ DEBUG: CTPFILES L1 menu GOT muctpi nbits");
            }
            catch(std::exception& e){ ATH_MSG_ERROR("MUCTPI DQ DEBUG: CTPFILES exception: "<<e.what()); }
            catch(...){ ATH_MSG_ERROR("MUCTPI DQ DEBUG: CTPFILES: Unknown exception"); }
        }

    }//if nbits not set

    //slices
    std::vector< LVL1::MuCTPIBits::Slice > slices;
    LVL1::MuCTPIBits::Slice slice;
    bool firstSlice=true;
    std::vector<size_t> errorBits;    
    uint64_t sliceMultiplicity=0;//grouping the 3 multiplicity words, to be processed at the end of the slice

    for( uint32_t iWord = 0; iWord < ndata; ++iWord, ++it_data ) {

        //for each word, get it, find type, and add in Slice struct.
        uint32_t word =  static_cast< uint32_t >( *it_data );
        ATH_MSG_DEBUG("MUCTPI raw word " << iWord << ": 0x" << std::hex << word << std::dec);
        LVL1::MuCTPIBits::WordType wordType = LVL1::MuCTPIBits::getWordType(word);

        switch (wordType) {
        case LVL1::MuCTPIBits::WordType::Timeslice: {

            ATH_MSG_WARNING(" MUCTPI DQ DEBUG: Timeslice found: "<< std::hex << word);

            //add previous slice if any
            if(!firstSlice)
            {
                ATH_MSG_WARNING(" MUCTPI DQ DEBUG: new timeslice found (pushing)");
                slices.push_back(slice);
            }
            else
                firstSlice=false;

            //make new slice (to be improved, since "new" will give pointer)
            LVL1::MuCTPIBits::Slice s;
            slice =  s;

            const auto header = LVL1::MuCTPIBits::timesliceHeader(word);
            ATH_MSG_DEBUG("This is a timeslice header word with BCID=" << header.bcid
                          << ", NTOB=" << header.tobCount << ", NCAND=" << header.candCount);
            slice.bcid  = header.bcid;
            slice.nCand = header.candCount;
            slice.nTOB  = header.tobCount;
            break;
        }
        case LVL1::MuCTPIBits::WordType::Multiplicity: {
            uint32_t tmNum = LVL1::MuCTPIBits::multiplicityWordNumber(word);
            ATH_MSG_DEBUG("This is a multiplicity word #" << tmNum);

            if(m_muctpi_Nbits.size()==32)
            {
                //fill mult word into temp container until 3rd word is found
                if(tmNum==1)
                    sliceMultiplicity |= LVL1::MuCTPIBits::maskedWord(word,LVL1::MuCTPIBits::RUN3_MULTIPLICITY_PART1_SHIFT, LVL1::MuCTPIBits::RUN3_MULTIPLICITY_PART1_MASK);
                else if(tmNum==2)
                    sliceMultiplicity |= LVL1::MuCTPIBits::maskedWord(word,LVL1::MuCTPIBits::RUN3_MULTIPLICITY_PART2_SHIFT, LVL1::MuCTPIBits::RUN3_MULTIPLICITY_PART2_MASK);
                else if(tmNum==3)
                    sliceMultiplicity |= LVL1::MuCTPIBits::maskedWord(word,LVL1::MuCTPIBits::RUN3_MULTIPLICITY_PART3_SHIFT, LVL1::MuCTPIBits::RUN3_MULTIPLICITY_PART3_MASK);

                //flags from third word
                //AND: process multiplicity for the slice!!!
                if(tmNum==3)
                {
                    slice.mlt.nswMon       = LVL1::MuCTPIBits::maskedWord(word,LVL1::MuCTPIBits::RUN3_NSW_MONITORING_TRIGGER_SHIFT, LVL1::MuCTPIBits::RUN3_NSW_MONITORING_TRIGGER_MASK);
                    slice.mlt.candOverflow =  LVL1::MuCTPIBits::maskedWord(word,LVL1::MuCTPIBits::RUN3_MULTIPLICITY_OVERFLOW_SHIFT, LVL1::MuCTPIBits::RUN3_MULTIPLICITY_OVERFLOW_MASK);

                    //process the long mult word into 32 mlt thr counters
                    for(uint iThr=0;iThr<m_muctpi_Nbits.size();iThr++)
                    {
                        uint thismask=0;
                        if(m_muctpi_Nbits[iThr]==1)
                            thismask=0x1;
                        else if(m_muctpi_Nbits[iThr]==2)
                            thismask=0x3;
                        else if(m_muctpi_Nbits[iThr]==3)
                            thismask=0x7;

                        //keep only the part of the 64bit word corresponding to the nbits value
                        slice.mlt.cnt.push_back( sliceMultiplicity & thismask);
                        //"throw away" the part of the 64bit word that we just used
                        sliceMultiplicity >>= m_muctpi_Nbits[iThr];
                    }

                    sliceMultiplicity=0;//cleaning just in case..
                }

            }
            else
                ATH_MSG_WARNING("MUCTPI DQ DEBUG: skipping Mult processing, no nbits defined");

            break;
        }
        case LVL1::MuCTPIBits::WordType::Candidate: {
          ATH_MSG_DEBUG("This is a RoI candidate word");

          //consider adding "using namespace LVL1::MuCTPIBits::" in this function

          LVL1::MuCTPIBits::Cand thiscand;
          thiscand.errorFlag = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_WORD_SECTORERRORFLAG_SHIFT, LVL1::MuCTPIBits::RUN3_CAND_WORD_SECTORERRORFLAG_MASK);
          thiscand.type = LVL1::MuCTPIBits::getSubsysID(word);
          thiscand.side = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_SUBSYS_HEMISPHERE_SHIFT, LVL1::MuCTPIBits::RUN3_SUBSYS_HEMISPHERE_MASK);
          if(thiscand.type==LVL1::MuCTPIBits::SubsysID::Endcap)
              thiscand.num = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_SECTORID_SHIFT, LVL1::MuCTPIBits::ENDCAP_SECTORID_MASK);
          else
              thiscand.num = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_SECTORID_SHIFT, LVL1::MuCTPIBits::BARREL_SECTORID_MASK);//same as FW
          thiscand.vetoFlag = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_WORD_VETO_SHIFT, LVL1::MuCTPIBits::RUN3_CAND_WORD_VETO_MASK);
          if(thiscand.type==LVL1::MuCTPIBits::SubsysID::Barrel)
          {
              thiscand.candFlag_phiOverlap = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_BA_PHIOVERLAP_SHIFT, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_BA_PHIOVERLAP_MASK);
              thiscand.candFlag_gt1CandRoi = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_BA_GT1ROI_SHIFT, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_BA_GT1ROI_MASK);
          }
          else
          {
              thiscand.candFlag_GoodMF = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_GOODMF_SHIFT, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_GOODMF_MASK);
              thiscand.candFlag_InnerCoin = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_INNERCOIN_SHIFT, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_INNERCOIN_MASK);
              thiscand.candFlag_BW23 = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_BW23_SHIFT, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_BW23_MASK);
              thiscand.candFlag_Charge = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_CHARGE_SHIFT, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_CHARGE_MASK);
          }
          thiscand.pt = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_WORD_PT_SHIFT, LVL1::MuCTPIBits::RUN3_CAND_WORD_PT_MASK);
          thiscand.roi = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_WORD_ROI_SHIFT, LVL1::MuCTPIBits::RUN3_CAND_WORD_ROI_MASK);

          slice.cand.push_back(thiscand);
          break;
        }
        case LVL1::MuCTPIBits::WordType::Topo: {
          ATH_MSG_DEBUG("This is a Topo TOB word "<< std::hex << word);

          LVL1::MuCTPIBits::TOB thistob;
          thistob.side = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_TOPO_WORD_HEMI_SHIFT, LVL1::MuCTPIBits::RUN3_TOPO_WORD_HEMI_MASK);
          //tob flags are exact same definition as cand flags
          thistob.candFlag_GoodMF = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_GOODMF_SHIFT, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_GOODMF_MASK);
          thistob.candFlag_InnerCoin = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_INNERCOIN_SHIFT, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_INNERCOIN_MASK);
          thistob.candFlag_BW23 = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_BW23_SHIFT, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_BW23_MASK);
          thistob.candFlag_Charge = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_CHARGE_SHIFT, LVL1::MuCTPIBits::RUN3_CAND_WORD_CANDFLAGS_ECFW_CHARGE_MASK);
          thistob.pt = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_TOPO_WORD_PT_SHIFT, LVL1::MuCTPIBits::RUN3_TOPO_WORD_PT_MASK);
          thistob.etaRaw = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_TOPO_WORD_ETA_SHIFT, LVL1::MuCTPIBits::RUN3_TOPO_WORD_ETA_MASK);
          thistob.phiRaw = LVL1::MuCTPIBits::maskedWord(word, LVL1::MuCTPIBits::RUN3_TOPO_WORD_PHI_SHIFT, LVL1::MuCTPIBits::RUN3_TOPO_WORD_PHI_MASK);

          slice.tob.push_back(thistob);
          break;
        }
        case LVL1::MuCTPIBits::WordType::Status: {
          ATH_MSG_DEBUG("This is a status word"<< std::hex << word);
          errorBits = LVL1::MuCTPIBits::getDataStatusWordErrors(word);
          if (!errorBits.empty()) {
            ATH_MSG_DEBUG("MUCTPI ROD data flagged with errors. The data status word is 0x" << std::hex << word << std::dec);
            for (size_t bit : errorBits) {
              ATH_MSG_DEBUG("Error bit " << bit << ": " << LVL1::MuCTPIBits::DataStatusWordErrors.at(bit));
            }
          }
          break;
        }
        default: {
          ATH_MSG_ERROR("The MUCTPI word 0x" << std::hex << word << std::dec << " does not match any known word type");
          return StatusCode::FAILURE;
        }
        }
    }

    //add last timeslice in vector, since there is no end-slice flag
    ATH_MSG_WARNING(" MUCTPI DQ DEBUG: out of words (pushing last slice)");
    slices.push_back( slice );

    // create MuCTPI RDO
    result = new MuCTPI_RDO( std::move(slices), std::move(errorBits) );
    return StatusCode::SUCCESS;
  }

  ATH_MSG_ERROR("Wrong ROD ID found in the MuCTPI ROB fragment!");
  return StatusCode::FAILURE;
}
