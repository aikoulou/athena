/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/
#include "BSMonitoringAlg.h"

TrigT1CTMonitoring::BSMonitoringAlgorithm::BSMonitoringAlgorithm( const std::string& name, ISvcLocator* pSvcLocator )
  : AthMonitorAlgorithm(name,pSvcLocator){}

StatusCode TrigT1CTMonitoring::BSMonitoringAlgorithm::initialize() {

  ATH_MSG_INFO("Initializing " << name());
  ATH_MSG_DEBUG("CTPmonitoring BSMonitorAlgorith::initialize");
  ATH_MSG_DEBUG("Package Name "<< m_packageName);
  // connect to RPC and TGC RoI tools
  //is this NOT used at all??? check the old code about these tools! - then remove or reactivate
  /*if ( m_processMuctpi ) {
    ATH_CHECK( m_rpcRoiTool.retrieve() );
    ATH_CHECK( m_tgcRoiTool.retrieve() );
    }*/
  
  ATH_CHECK( m_MuCTPI_RDOKey.initialize(m_processMuctpi) );
  //ATH_CHECK( m_MuCTPI_RIOKey.initialize(m_processMuctpi && m_processMuctpiRIO && ! m_runOnESD) );
  ATH_CHECK( m_CTP_RDOKey.initialize(m_processCTP) );
  ATH_CHECK( m_CTP_RIOKey.initialize(m_processCTP && ! m_runOnESD) );
  ATH_CHECK( m_CTP_RDO_RerunKey.initialize(m_processCTP && m_compareRerun) );
  ATH_CHECK( m_RPCContainerKey.initialize(m_processMuctpi) );
  ATH_CHECK( m_TGCContainerKey.initialize(m_processMuctpi) );
  ATH_CHECK( m_EventInfoKey.initialize() );


  //COOL access
  ATH_CHECK( m_LBLBFolderInputKey.initialize(!m_isSim) );
  ATH_CHECK( m_FILLSTATEFolderInputKey.initialize(!m_isSim) );
  ATH_CHECK( m_DataTakingModeFolderInputKey.initialize(!m_isSim) );

  ATH_MSG_INFO("Printing the BSMonitoringAlgorithm Configuration: ");
  ATH_MSG_INFO("InclusiveTriggerThresholds: " << m_inclusiveTriggerThresholds);
  ATH_MSG_INFO("ProcessMuctpiData: " << m_processMuctpi);
  ATH_MSG_INFO("ProcessMuctpiDataRIO: " << m_processMuctpiRIO);
  ATH_MSG_INFO("RunOnESD: " << m_runOnESD);
  ATH_MSG_INFO("CompareRerun: " << m_compareRerun);

  ATH_MSG_INFO("ProcessCTPData: " << m_processCTP);
  if (!m_isSim) ATH_MSG_INFO("Simulation or Data?: DATA");
  else ATH_MSG_INFO("Simulation or Data?: SIMULATION");

  return AthMonitorAlgorithm::initialize();
}

StatusCode TrigT1CTMonitoring::BSMonitoringAlgorithm::fillHistograms( const EventContext& ctx ) const {
  using namespace Monitored;
  ATH_MSG_DEBUG("CTPmonitoring BSMonitorAlgorithm::fillHistograms");
  try {
    ATH_MSG_DEBUG( "begin fillHistograms()");

    // Now see what exists in  StoreGate...
    const MuCTPI_RDO* theMuCTPI_RDO = 0;
    //const MuCTPI_RIO* theMuCTPI_RIO = 0;
    const CTP_RDO* theCTP_RDO = 0;
    const CTP_RIO* theCTP_RIO = 0;
    const Muon::TgcCoinDataContainer* theTGCContainer = 0;
    const RpcSectorLogicContainer* theRPCContainer = 0;

    //bool validMuCTPI_RIO = true;
    bool validMuCTPI_RDO = true;
    bool validCTP_RIO = true;
    bool validCTP_RDO = true;
    bool validTGCContainer = true;
    bool validRPCContainer = true;
    int numberOfInvalidFragments = 0;

    //ERROR histos
    auto errorSummaryCTPX = Monitored::Scalar<int>("errorSummaryCTPX",0);
    auto errorSummaryCTPY = Monitored::Scalar<int>("errorSummaryCTPY",0);
    auto errorSummaryPerLumiBlockCTPX = Monitored::Scalar<int>("errorSummaryPerLumiBlockCTPX",0);
    auto errorSummaryPerLumiBlockCTPY = Monitored::Scalar<int>("errorSummaryPerLumiBlockCTPY",0);
    auto errorPerLumiBlockX = Monitored::Scalar<int>("errorPerLumiBlockX",0);
    auto incompleteFragmentTypeX = Monitored::Scalar<int>("incompleteFragmentTypeX",0);
    auto incompleteFragmentTypeY = Monitored::Scalar<int>("incompleteFragmentTypeY",0);

    auto eventInfo = GetEventInfo(ctx);

    if (m_processMuctpi) {
      ATH_MSG_DEBUG( "CTPMON fillHistograms() m_processMuctpi");
      theMuCTPI_RDO = SG::get(m_MuCTPI_RDOKey, ctx);
      if (!theMuCTPI_RDO) {
	ATH_MSG_WARNING( "Could not find \"" << m_MuCTPI_RDOKey.key() << "\" in StoreGate");
	validMuCTPI_RDO = false;
	++numberOfInvalidFragments;
      }
      // now try to get RPC and TGC SL output for comparisons
      theRPCContainer = SG::get(m_RPCContainerKey, ctx);
      if (!theRPCContainer) {
	ATH_MSG_WARNING( "Could not find RPC container in StoreGate");
	validRPCContainer = false;
      }
      theTGCContainer = SG::get(m_TGCContainerKey, ctx);
      if (!theTGCContainer) {
	ATH_MSG_WARNING( "Could not find TGC container in StoreGate");
	validTGCContainer = false;
      }
    }
    
    if (m_processCTP) {
      ATH_MSG_DEBUG( "CTPMON fillHistograms() m_processCTP");
      theCTP_RDO = SG::get(m_CTP_RDOKey, ctx);
      //ATH_MSG_INFO( "CTPMON theCTP_RDO->isValid()" << theCTP_RDO->isValid());
      if (!theCTP_RDO) {
	ATH_MSG_WARNING( "Could not find \"" << m_CTP_RDOKey.key() << "\" in StoreGate");
	validCTP_RDO = false;
	++numberOfInvalidFragments;
      }
      if (!m_runOnESD) {
	theCTP_RIO = SG::get(m_CTP_RIOKey, ctx);
	if (!theCTP_RIO) {
	  ATH_MSG_WARNING( "Could not find \"" << m_CTP_RIOKey.key() << "\" in StoreGate");
	  validCTP_RIO = false;
	  ++numberOfInvalidFragments;
	}
        ATH_MSG_DEBUG( "validCTP_RIO: " << validCTP_RIO );
      }
    }

    bool incompleteEvent = false;
    int runNumber = 0;
    unsigned int eventNumber = 0;
    uint32_t currentLumiBlock = 0;

    if (eventInfo->runNumber()) {
      currentLumiBlock = eventInfo->lumiBlock();
      runNumber = eventInfo->runNumber();
      eventNumber = eventInfo->eventNumber();
      //lumiBlockOfPreviousEvent = currentLumiBlock;
      //currentLumiBlock =  eventInfo->lumiBlock();
      incompleteEvent = eventInfo->eventFlags(xAOD::EventInfo::Core) & 0x40000;
      ATH_MSG_DEBUG( "Successfully retrieved EventInfo (run: " << runNumber << ", event: " << eventNumber << ")");
    }
    /*
    else {
      ATH_MSG_WARNING( "Could not retrieve EventInfo from StoreGate => run# = event# = 0, LB# = 99");
      lumiBlockOfPreviousEvent = currentLumiBlock;
      currentLumiBlock = 99; // dummy LB in case EventInfo is not available - prevents DQ defect flagging with LB# 0...
    }
    */
    if ( incompleteEvent ) {
      ATH_MSG_WARNING( "Incomplete event according to EventInfo flag");
      incompleteFragmentTypeX = 4;
      fill(m_packageName, incompleteFragmentTypeX);
    }

    //bool l1ctObjectMissingInStoreGate = ( !validCTP_RDO || !validCTP_RIO || !validMuCTPI_RDO || !validMuCTPI_RIO || !validRoIBResult );
    bool l1ctObjectMissingInStoreGate = ( !validCTP_RDO || !validCTP_RIO || !validMuCTPI_RDO);
    if ( l1ctObjectMissingInStoreGate ) {
      ATH_MSG_WARNING( "At least one L1CT object is missing in SG");
    }

    //dumpData(theCTP_RDO, theCTP_RIO, theMuCTPI_RDO, theMuCTPI_RIO, roIBResult, ctx);
    dumpData(theCTP_RDO, /*theCTP_RIO,*/ theMuCTPI_RDO, ctx);
    if ( m_processCTP ) {
      if ( validCTP_RDO ) {
	const std::vector<uint32_t> &cDataWords = theCTP_RDO->getDataWords();
	if ( cDataWords.size() == 0 ) {
	  ATH_MSG_WARNING( "CTP_RDO is empty, ignoring CTP");
	  validCTP_RDO = false;
	}
      }

      if ( validCTP_RIO ) {
	if ( !m_runOnESD && (theCTP_RIO->getDetectorEventType() & 0xffff) == 0 ) {//LB == 0 only if RIO is empty
	  ATH_MSG_WARNING( "CTP_RIO is not valid, ignoring CTP");
	  validCTP_RIO = false;
	}
      }
    }

    if ( m_processMuctpi ) {
      if ( validMuCTPI_RDO ) {
	MuCTPI_MultiplicityWord_Decoder multWord(theMuCTPI_RDO->candidateMultiplicity(), m_inclusiveTriggerThresholds);
	// consider the fragment incomplete if the number of data words is less than
	// the reported number of candidates (zero words valid for muon-less events!)
	if (theMuCTPI_RDO->dataWord().size() < multWord.getNCandidates()) {
	  //patrick ATH_MSG_INFO
	  ATH_MSG_DEBUG("MuCTPI_RDO reports " << multWord.getNCandidates()
		       //the following gives 0 for now - but why???
		       << "  candidates, but there are only " << theMuCTPI_RDO->dataWord().size()
		       << " data words, ignoring MuCTPI");
	  validMuCTPI_RDO = false;
	}
      }
      // Note: there's no simple way of checking the validity of the MUCTPI_RIO, so we don't for now.
    }

    // if at least one fragment is missing/incomplete, print out a summary
    // if (!validCTP_RDO || !validCTP_RIO || !validMuCTPI_RDO || !validMuCTPI_RIO || !validRoIBResult) {
    if (!validCTP_RDO || !validCTP_RIO) {
      ATH_MSG_WARNING( "At least one missing/invalid L1CT fragment detected");
      ATH_MSG_WARNING( "CTP_RDO: " << validCTP_RDO << ", CTP_RIO: " << validCTP_RIO);
      //ATH_MSG_INFO( "Run number: " << eventInfo->runNumber() << ", Event: " << eventInfo->eventNumber() << ", LB: " << eventInfo->lumiBlock() );
      if (validCTP_RIO) {
	ATH_MSG_WARNING( "CTP_RIO says LB: " << (theCTP_RIO->getDetectorEventType() & 0xffff)
                         << ", L1ID: " << std::dec << theCTP_RIO->getLvl1Id()
                         << " (HEX: " << std::hex << theCTP_RIO->getLvl1Id() << ")" << std::dec
                         << ", BCID: " << theCTP_RIO->getBCID());
	ATH_MSG_WARNING( "CTP_RIO says LB: " << (theCTP_RIO->getDetectorEventType() & 0xffff));
	ATH_MSG_WARNING( "CTP_RIO says L1ID: " << std::dec << theCTP_RIO->getLvl1Id());
	ATH_MSG_WARNING( "CTP_RIO says HEX: " << std::hex << theCTP_RIO->getLvl1Id() << ")" << std::dec);
	ATH_MSG_WARNING( "CTP_RIO says BCID: " << theCTP_RIO->getBCID());
	ATH_MSG_WARNING("in: validCTP_RIO - survived this? crashing now?");
      }
      else if (eventInfo->runNumber()) {
	ATH_MSG_WARNING( "CTP_RIO missing, EventInfo says LB: " << eventInfo->lumiBlock() 
			 << ", BCID: " << eventInfo->bcid()); 
      }
      else {
	ATH_MSG_WARNING( "Not printing event details since both CTP_RIO and EventInfo objects are missing");
      }

      // only fill error-per-LB histograms if L1CT fragments are missing and global incomplete-event flag
      // from EventInfo does not say that the event is incomplete
      if ( !incompleteEvent ) {
    errorSummaryPerLumiBlockCTPX = currentLumiBlock;
    errorSummaryPerLumiBlockCTPY = 9;
    fill(m_packageName, errorSummaryPerLumiBlockCTPX, errorSummaryPerLumiBlockCTPY);
	errorPerLumiBlockX = currentLumiBlock;
	fill(m_packageName, errorPerLumiBlockX);
      }
      errorSummaryCTPX = 9;
      errorSummaryCTPY = 1;
      fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);

      if (!validCTP_RIO) {
	incompleteFragmentTypeX = 0;
	fill(m_packageName, incompleteFragmentTypeX);
      }
      if (!validCTP_RDO) {
	incompleteFragmentTypeX = 1;
	fill(m_packageName, incompleteFragmentTypeX);
      }
      /*
      if (!validMuCTPI_RIO) {
        incompleteFragmentTypeX = 2;
	fill(m_packageName, incompleteFragmentTypeX);
	}*/
      if (!validMuCTPI_RDO) {
        incompleteFragmentTypeX = 2;
	fill(m_packageName, incompleteFragmentTypeX);
      }
      if (!validTGCContainer) {
        incompleteFragmentTypeX = 4;
	fill(m_packageName, incompleteFragmentTypeX);
      }
      if (!validRPCContainer) {
        incompleteFragmentTypeX = 5;
	fill(m_packageName, incompleteFragmentTypeX);
      }
    }
    else { // since errorSummary holds error _rate_, also fill when there are no errors
      errorSummaryCTPX = 9;
      errorSummaryCTPY = 0;
      fill(m_packageName, errorSummaryCTPX);
    }

    // if the event is incomplete (missing L1CT objects or according to EventInfo), skip filling the rest of the histograms
    //if ( !validCTP_RDO || !validCTP_RIO || !validMuCTPI_RDO || !validMuCTPI_RIO || incompleteEvent ) {
    if ( !validCTP_RDO || !validCTP_RIO || !validMuCTPI_RDO || incompleteEvent ) {
      ATH_MSG_WARNING( "Event incomplete, will skip filling of all non-error histograms");
      //comment patrick: why was this here if later the single validities are checked?
      //bc of validCTP_RIO
      //return StatusCode::SUCCESS;
    }

    /*
     * Process and fill data
     */
    if (m_processCTP && validCTP_RDO && validCTP_RIO) {
      ATH_MSG_DEBUG( "CTPMON before begin doCtp()");
      doCtp(theCTP_RDO, theCTP_RIO, ctx);
    }

    //if (m_processMuctpi && m_processCTP && validCTP_RDO && validCTP_RIO && validMuCTPI_RDO && validMuCTPI_RIO) {
    if (m_processMuctpi && m_processCTP && validCTP_RDO && validMuCTPI_RDO ) {
      ATH_MSG_DEBUG( "CTPMON before begin doCtpMuctpi()");
      //doCtpMuctpi(theCTP_RDO, theCTP_RIO, theMuCTPI_RDO, theMuCTPI_RIO, ctx);
      doCtpMuctpi(theCTP_RDO, theMuCTPI_RDO, ctx);
    }

    //if (m_processMuctpi && validMuCTPI_RDO && validMuCTPI_RIO && validTGCContainer && validRPCContainer) {
    if (m_processMuctpi && validMuCTPI_RDO && validTGCContainer && validRPCContainer) {
      ATH_MSG_DEBUG( "CTPMON before begin doMuctpi()");
      //doMuctpi(theMuCTPI_RDO, theMuCTPI_RIO, theRPCContainer, theTGCContainer, ctx);
      doMuctpi(theMuCTPI_RDO, theRPCContainer, theTGCContainer, ctx);
    }
    //++m_eventCount;
    ATH_MSG_DEBUG( "end fillHistograms()");
    return StatusCode::SUCCESS;
  }
  catch(const std::exception & e) {
    std::cerr << "Caught standard C++ exception: " << e.what() << " from fillHistograms()" << std::endl;
    return StatusCode::FAILURE;
  }
  }


void
TrigT1CTMonitoring::BSMonitoringAlgorithm::doMuctpi(const MuCTPI_RDO* theMuCTPI_RDO, //const MuCTPI_RIO* theMuCTPI_RIO,
						    const RpcSectorLogicContainer* theRPCContainer,
						    const Muon::TgcCoinDataContainer* theTGCContainer,
						    const EventContext& ctx) const
{
  ATH_MSG_DEBUG( "CTPMON begin doMuctpi()");
  //ERROR vector  - COMMON
  auto errorPerLumiBlockX = Monitored::Scalar<int>("errorPerLumiBlockX",0);
  //ERROR vectors - MUCTPI
  auto errorSummaryMUCTPIX = Monitored::Scalar<int>("errorSummaryMUCTPIX",0);
  auto errorSummaryMUCTPIY = Monitored::Scalar<int>("errorSummaryMUCTPIY",0);
  auto errorSummaryPerLumiBlockMUCTPIX = Monitored::Scalar<int>("errorSummaryPerLumiBlockMUCTPIX",0);
  auto errorSummaryPerLumiBlockMUCTPIY = Monitored::Scalar<int>("errorSummaryPerLumiBlockMUCTPIY",0);
  auto statusDataWordMUCTPIX = Monitored::Scalar<int>("statusDataWordMUCTPIX",0);
  auto statusDataWordMUCTPIY = Monitored::Scalar<int>("statusDataWordMUCTPIY",0);
  auto statusDataWordPerLumiBlockMUCTPIX = Monitored::Scalar<int>("statusDataWordPerLumiBlockMUCTPIX",0);
  auto statusDataWordPerLumiBlockMUCTPIY = Monitored::Scalar<int>("statusDataWordPerLumiBlockMUCTPIY",0);
  // MUCTPI-specific
  //auto deltaBcidX = Monitored::Scalar<int>("deltaBcidX",0); //unused
  auto candPtBAX = Monitored::Scalar<int>("candPtBAX",0);
  auto candPtECX = Monitored::Scalar<int>("candPtECX",0);
  auto candPtFWX = Monitored::Scalar<int>("candPtFWX",0);
  auto candRoiVsSLBACentralSliceX = Monitored::Scalar<int>("candRoiVsSLBACentralSliceX",0);
  auto candRoiVsSLBACentralSliceY = Monitored::Scalar<int>("candRoiVsSLBACentralSliceY",0);
  auto candRoiVsSLECCentralSliceX = Monitored::Scalar<int>("candRoiVsSLECCentralSliceX",0);
  auto candRoiVsSLECCentralSliceY = Monitored::Scalar<int>("candRoiVsSLECCentralSliceY",0);
  auto candRoiVsSLFWCentralSliceX = Monitored::Scalar<int>("candRoiVsSLFWCentralSliceX",0);
  auto candRoiVsSLFWCentralSliceY = Monitored::Scalar<int>("candRoiVsSLFWCentralSliceY",0);
  auto candRoiVsSLBAOtherSliceX = Monitored::Scalar<int>("candRoiVsSLBAOtherSliceX",0);
  auto candRoiVsSLBAOtherSliceY = Monitored::Scalar<int>("candRoiVsSLBAOtherSliceY",0);
  auto candRoiVsSLECOtherSliceX = Monitored::Scalar<int>("candRoiVsSLECOtherSliceX",0);
  auto candRoiVsSLECOtherSliceY = Monitored::Scalar<int>("candRoiVsSLECOtherSliceY",0);
  auto candRoiVsSLFWOtherSliceX = Monitored::Scalar<int>("candRoiVsSLFWOtherSliceX",0);
  auto candRoiVsSLFWOtherSliceY = Monitored::Scalar<int>("candRoiVsSLFWOtherSliceY",0);
  auto candCandFlagsVsSLBACentralSliceX = Monitored::Scalar<int>("candCandFlagsVsSLBACentralSliceX",0);
  auto candCandFlagsVsSLBACentralSliceY = Monitored::Scalar<int>("candCandFlagsVsSLBACentralSliceY",0);
  auto candCandFlagsVsSLECCentralSliceX = Monitored::Scalar<int>("candCandFlagsVsSLECCentralSliceX",0);
  auto candCandFlagsVsSLECCentralSliceY = Monitored::Scalar<int>("candCandFlagsVsSLECCentralSliceY",0);
  auto candCandFlagsVsSLFWCentralSliceX = Monitored::Scalar<int>("candCandFlagsVsSLFWCentralSliceX",0);
  auto candCandFlagsVsSLFWCentralSliceY = Monitored::Scalar<int>("candCandFlagsVsSLFWCentralSliceY",0);
  auto candErrorFlagVsSLBACentralSlicePerLBX = Monitored::Scalar<int>("candErrorFlagVsSLBACentralSlicePerLBX",0);
  auto candErrorFlagVsSLBACentralSlicePerLBY = Monitored::Scalar<int>("candErrorFlagVsSLBACentralSlicePerLBY",0);
  auto candErrorFlagVsSLECCentralSlicePerLBX = Monitored::Scalar<int>("candErrorFlagVsSLECCentralSlicePerLBX",0);
  auto candErrorFlagVsSLECCentralSlicePerLBY = Monitored::Scalar<int>("candErrorFlagVsSLECCentralSlicePerLBY",0);
  auto candErrorFlagVsSLFWCentralSlicePerLBX = Monitored::Scalar<int>("candErrorFlagVsSLFWCentralSlicePerLBX",0);
  auto candErrorFlagVsSLFWCentralSlicePerLBY = Monitored::Scalar<int>("candErrorFlagVsSLFWCentralSlicePerLBY",0);
  auto candSliceVsSLBAX = Monitored::Scalar<int>("candSliceVsSLBAX",0);
  auto candSliceVsSLBAY = Monitored::Scalar<int>("candSliceVsSLBAY",0);
  auto candSliceVsSLECX = Monitored::Scalar<int>("candSliceVsSLECX",0);
  auto candSliceVsSLECY = Monitored::Scalar<int>("candSliceVsSLECY",0);
  auto candSliceVsSLFWX = Monitored::Scalar<int>("candSliceVsSLFWX",0);
  auto candSliceVsSLFWY = Monitored::Scalar<int>("candSliceVsSLFWY",0);
  //TOB
  auto tobEtaPhiAX = Monitored::Scalar<int>("tobEtaPhiAX",0);
  auto tobEtaPhiAY = Monitored::Scalar<int>("tobEtaPhiAY",0);
  auto tobEtaPhiA_GoodMFX = Monitored::Scalar<int>("tobEtaPhiA_GoodMFX",0);
  auto tobEtaPhiA_GoodMFY = Monitored::Scalar<int>("tobEtaPhiA_GoodMFY",0);
  auto tobEtaPhiA_InnerCoinX = Monitored::Scalar<int>("tobEtaPhiA_InnerCoinX",0);
  auto tobEtaPhiA_InnerCoinY = Monitored::Scalar<int>("tobEtaPhiA_InnerCoinY",0);
  auto tobEtaPhiA_BW23X = Monitored::Scalar<int>("tobEtaPhiA_BW23X",0);
  auto tobEtaPhiA_BW23Y = Monitored::Scalar<int>("tobEtaPhiA_BW23Y",0);
  auto tobEtaPhiA_ChargeX = Monitored::Scalar<int>("tobEtaPhiA_ChargeX",0);
  auto tobEtaPhiA_ChargeY = Monitored::Scalar<int>("tobEtaPhiA_ChargeY",0);
  auto tobEtaPhiCX = Monitored::Scalar<int>("tobEtaPhiCX",0);
  auto tobEtaPhiCY = Monitored::Scalar<int>("tobEtaPhiCY",0);
  auto tobEtaPhiC_GoodMFX = Monitored::Scalar<int>("tobEtaPhiC_GoodMFX",0);
  auto tobEtaPhiC_GoodMFY = Monitored::Scalar<int>("tobEtaPhiC_GoodMFY",0);
  auto tobEtaPhiC_InnerCoinX = Monitored::Scalar<int>("tobEtaPhiC_InnerCoinX",0);
  auto tobEtaPhiC_InnerCoinY = Monitored::Scalar<int>("tobEtaPhiC_InnerCoinY",0);
  auto tobEtaPhiC_BW23X = Monitored::Scalar<int>("tobEtaPhiC_BW23X",0);
  auto tobEtaPhiC_BW23Y = Monitored::Scalar<int>("tobEtaPhiC_BW23Y",0);
  auto tobEtaPhiC_ChargeX = Monitored::Scalar<int>("tobEtaPhiC_ChargeX",0);
  auto tobEtaPhiC_ChargeY = Monitored::Scalar<int>("tobEtaPhiC_ChargeY",0);
  auto tobPtVsEtaAX = Monitored::Scalar<int>("tobPtVsEtaAX",0);
  auto tobPtVsEtaAY = Monitored::Scalar<int>("tobPtVsEtaAY",0);
  auto tobPtVsPhiAX = Monitored::Scalar<int>("tobPtVsPhiAX",0);
  auto tobPtVsPhiAY = Monitored::Scalar<int>("tobPtVsPhiAY",0);
  auto tobPtVsEtaCX = Monitored::Scalar<int>("tobPtVsEtaCX",0);
  auto tobPtVsEtaCY = Monitored::Scalar<int>("tobPtVsEtaCY",0);
  auto tobPtVsPhiCX = Monitored::Scalar<int>("tobPtVsPhiCX",0);
  auto tobPtVsPhiCY = Monitored::Scalar<int>("tobPtVsPhiCY",0);
  //mlt
  auto multX = Monitored::Scalar<int>("multX",0);
  auto multPerLBX = Monitored::Scalar<int>("multPerLBX",0);
  auto multPerLBY = Monitored::Scalar<int>("multPerLBY",0);
  auto multSliceVsMultX = Monitored::Scalar<int>("multSliceVsMultX",0);
  auto multSliceVsMultY = Monitored::Scalar<int>("multSliceVsMultY",0);




  //get event info
  auto eventInfo = GetEventInfo(ctx);
  uint32_t currentLumiBlock = eventInfo->lumiBlock();

  //get muctpi fragment data in the form of a vector of timeslices
  const std::vector<LVL1::MuCTPIBits::Slice> &slices = theMuCTPI_RDO->slices();

  //https://gitlab.cern.ch/atlas/athena/-/blob/master/Trigger/TrigT1/TrigT1Result/src/MuCTPI_DataWord_Decoder.cxx
  //https://gitlab.cern.ch/atlas/athena/-/blob/master/Trigger/TrigT1/TrigT1Result/TrigT1Result/MuCTPI_DataWord_Decoder.h


  //data is grouped in slices:
  for(uint iSlice=0;iSlice<theMuCTPI_RDO->slices().size();iSlice++)
  {

      ATH_MSG_DEBUG( "MUCTPI DQ DEBUG: iSlice = "<<iSlice);

      //assuming only 1,3,5,7 possible slices:
      bool isCentralSlice=false;
      if(theMuCTPI_RDO->slices().size()==1)
          isCentralSlice=true;
      else
          isCentralSlice = (theMuCTPI_RDO->slices().size()-1)/2 == iSlice;

      //-------------------------------------------------
      // HEADER
      //-------------------------------------------------
      //uint header_bcid = slices[iSlice].bcid; //no use for it?
      uint header_tobCount = slices[iSlice].nTOB;  //check during cand/topo section below
      uint header_candCount = slices[iSlice].nCand;//check during cand/topo section below

      ///bcid check: header against RDO bcid
      // actually skipping this, since it is checked online by HLT (MUCTPI_BCIDOffsetsWrtROB)

      //-------------------------------------------------
      // MULTIPLICITY
      //-------------------------------------------------

      ///MLT vector of up to 32 items:
      /// fill histo 1D total numbers
      /// fill histo 2D per LB
      /// fill histo 2D per (timeslice relative BCID)

      //flags from 3rd Mult word
      if(isCentralSlice)
      {

      }

      for(uint iThr=0;iThr<slices[iSlice].mlt.cnt.size();iThr++)
      {
          multX = iThr;
          fill(m_packageName, multX);
          multPerLBX = currentLumiBlock;
          multPerLBY = iThr;
          fill(m_packageName, multPerLBX, multPerLBY);

          multSliceVsMultY = iThr;
          if(isCentralSlice)
              multSliceVsMultX = 0;
          else
              multSliceVsMultX = 1;
          fill(m_packageName, multSliceVsMultX, multSliceVsMultY);
      }


      ///NSW mon trigger    todo?
      ///cand overflow flag todo?

      //-------------------------------------------------
      // CANDIDATES
      //-------------------------------------------------
      //Errors:
      ///if candCount!=candidate words
      if(header_candCount != slices[iSlice].cand.size())
      {
          errorSummaryMUCTPIX=2;
          fill(m_packageName, errorSummaryMUCTPIX);
          errorSummaryPerLumiBlockMUCTPIX=2;
          errorSummaryPerLumiBlockMUCTPIY=currentLumiBlock;
          fill(m_packageName, errorSummaryPerLumiBlockMUCTPIX, errorSummaryPerLumiBlockMUCTPIY);
          //ATH_MSG_WARNING( "MUCTPI DQ DEBUG: iSlice="<<iSlice << " header_candCount="<<header_candCount<<" slices[iSlice].cand.size()="<<slices[iSlice].cand.size() );
      }


      for(uint iCand=0;iCand<slices[iSlice].cand.size();iCand++)
      {
          /// 1D pT stats x3

          /// 2D roi vs SL (    central slice) x3
          /// 2D roi vs SL (non-central slice) x3

          /// 2D cand flags vs SL BA (2)
          /// 2D cand flags vs SL EC (4)
          /// 2D cand flags vs SL FW (4)

          /// 2D sec err flag per LB x3

          /// 2D fill SL (central "VS" non-central slice) x3
          /// todo: 1D fill ratio (central / non-central slice) x3 (hopefully can make the ratio on Han?)


          ATH_MSG_DEBUG( "MUCTPI DQ DEBUG: iCand="<<iCand << " type="<<(int)slices[iSlice].cand[iCand].type<< " num="<<(int)slices[iSlice].cand[iCand].num<< " pt="<<slices[iSlice].cand[iCand].pt);
          ATH_MSG_WARNING( "MUCTPI DQ DEBUG: iCand="<<iCand << " type="<<(int)slices[iSlice].cand[iCand].type<< " num="<<(int)slices[iSlice].cand[iCand].num<< " pt="<<slices[iSlice].cand[iCand].pt);

          if(slices[iSlice].cand[iCand].type==LVL1::MuCTPIBits::SubsysID::Barrel)
          {
              uint num = slices[iSlice].cand[iCand].num + 32*(1-slices[iSlice].cand[iCand].side);//offset side C;
              candPtBAX = slices[iSlice].cand[iCand].pt;
              fill(m_packageName, candPtBAX);

              if(isCentralSlice)
              {
                  candRoiVsSLBACentralSliceX = num;
                  candRoiVsSLBACentralSliceY = slices[iSlice].cand[iCand].roi;
                  fill(m_packageName, candRoiVsSLBACentralSliceX, candRoiVsSLBACentralSliceY);

                  candCandFlagsVsSLBACentralSliceX = num;
                  candCandFlagsVsSLBACentralSliceY = 0;
                  if(slices[iSlice].cand[iCand].candFlag_gt1CandRoi)
                      fill(m_packageName, candCandFlagsVsSLBACentralSliceX, candCandFlagsVsSLBACentralSliceY);

                  candCandFlagsVsSLBACentralSliceY = 1;
                  if(slices[iSlice].cand[iCand].candFlag_phiOverlap)
                      fill(m_packageName, candCandFlagsVsSLBACentralSliceX, candCandFlagsVsSLBACentralSliceY);

                  candErrorFlagVsSLBACentralSlicePerLBX = num;
                  candErrorFlagVsSLBACentralSlicePerLBY = currentLumiBlock;
                  if(slices[iSlice].cand[iCand].errorFlag)
                      fill(m_packageName, candErrorFlagVsSLBACentralSlicePerLBX, candErrorFlagVsSLBACentralSlicePerLBY);

                  candSliceVsSLBAX = num;
                  candSliceVsSLBAY = 0;
                  fill(m_packageName, candSliceVsSLBAX, candSliceVsSLBAY);
              }
              else
              {
                  candRoiVsSLBAOtherSliceX = slices[iSlice].cand[iCand].num + 32*(1-slices[iSlice].cand[iCand].side);//offset side C
                  candRoiVsSLBAOtherSliceY = slices[iSlice].cand[iCand].roi;
                  fill(m_packageName, candRoiVsSLBAOtherSliceX, candRoiVsSLBAOtherSliceY);

                  candSliceVsSLBAX = num;
                  candSliceVsSLBAY = 1;
                  fill(m_packageName, candSliceVsSLBAX, candSliceVsSLBAY);
              }
          }
          else if(slices[iSlice].cand[iCand].type==LVL1::MuCTPIBits::SubsysID::Endcap)
          {
              uint num = slices[iSlice].cand[iCand].num + 48*(1-slices[iSlice].cand[iCand].side);//offset side C;

              candPtECX = slices[iSlice].cand[iCand].pt;
              fill(m_packageName, candPtECX);

              if(isCentralSlice)
              {
                  candRoiVsSLECCentralSliceX = num;
                  candRoiVsSLECCentralSliceY = slices[iSlice].cand[iCand].roi;
                  fill(m_packageName, candRoiVsSLECCentralSliceX, candRoiVsSLECCentralSliceY);

                  candCandFlagsVsSLECCentralSliceX = num;
                  candCandFlagsVsSLECCentralSliceY = 0;
                  if(slices[iSlice].cand[iCand].candFlag_Charge)
                      fill(m_packageName, candCandFlagsVsSLECCentralSliceX, candCandFlagsVsSLECCentralSliceY);

                  candCandFlagsVsSLECCentralSliceY = 1;
                  if(slices[iSlice].cand[iCand].candFlag_BW23)
                      fill(m_packageName, candCandFlagsVsSLECCentralSliceX, candCandFlagsVsSLECCentralSliceY);

                  candCandFlagsVsSLECCentralSliceY = 2;
                  if(slices[iSlice].cand[iCand].candFlag_InnerCoin)
                      fill(m_packageName, candCandFlagsVsSLECCentralSliceX, candCandFlagsVsSLECCentralSliceY);

                  candCandFlagsVsSLECCentralSliceY = 3;
                  if(slices[iSlice].cand[iCand].candFlag_GoodMF)
                      fill(m_packageName, candCandFlagsVsSLECCentralSliceX, candCandFlagsVsSLECCentralSliceY);

                  candErrorFlagVsSLECCentralSlicePerLBX = num;
                  candErrorFlagVsSLECCentralSlicePerLBY = currentLumiBlock;
                  if(slices[iSlice].cand[iCand].errorFlag)
                      fill(m_packageName, candErrorFlagVsSLECCentralSlicePerLBX, candErrorFlagVsSLECCentralSlicePerLBY);

                  candSliceVsSLECX = num;
                  candSliceVsSLECY = 0;
                  fill(m_packageName, candSliceVsSLECX, candSliceVsSLECY);
              }
              else
              {
                  candRoiVsSLECOtherSliceX = slices[iSlice].cand[iCand].num + 48*(1-slices[iSlice].cand[iCand].side);//offset side C
                  candRoiVsSLECOtherSliceY = slices[iSlice].cand[iCand].roi;
                  fill(m_packageName, candRoiVsSLECOtherSliceX, candRoiVsSLECOtherSliceY);

                  candSliceVsSLECX = num;
                  candSliceVsSLECY = 1;
                  fill(m_packageName, candSliceVsSLECX, candSliceVsSLECY);
              }
          }
          else if(slices[iSlice].cand[iCand].type==LVL1::MuCTPIBits::SubsysID::Forward)
          {
              uint num = slices[iSlice].cand[iCand].num + 24*(1-slices[iSlice].cand[iCand].side);//offset side C;
              candPtFWX = slices[iSlice].cand[iCand].pt;
              fill(m_packageName, candPtFWX);

              if(isCentralSlice)
              {
                  candRoiVsSLFWCentralSliceX = num;
                  candRoiVsSLFWCentralSliceY = slices[iSlice].cand[iCand].roi;
                  fill(m_packageName, candRoiVsSLFWCentralSliceX, candRoiVsSLFWCentralSliceY);

                  candCandFlagsVsSLFWCentralSliceX = num;
                  candCandFlagsVsSLFWCentralSliceY = 0;
                  if(slices[iSlice].cand[iCand].candFlag_Charge)
                      fill(m_packageName, candCandFlagsVsSLFWCentralSliceX, candCandFlagsVsSLFWCentralSliceY);

                  candCandFlagsVsSLFWCentralSliceY = 1;
                  if(slices[iSlice].cand[iCand].candFlag_BW23)
                      fill(m_packageName, candCandFlagsVsSLFWCentralSliceX, candCandFlagsVsSLFWCentralSliceY);

                  candCandFlagsVsSLFWCentralSliceY = 2;
                  if(slices[iSlice].cand[iCand].candFlag_InnerCoin)
                      fill(m_packageName, candCandFlagsVsSLFWCentralSliceX, candCandFlagsVsSLFWCentralSliceY);

                  candCandFlagsVsSLFWCentralSliceY = 3;
                  if(slices[iSlice].cand[iCand].candFlag_GoodMF)
                      fill(m_packageName, candCandFlagsVsSLFWCentralSliceX, candCandFlagsVsSLECCentralSliceY);

                  candErrorFlagVsSLFWCentralSlicePerLBX = num;
                  candErrorFlagVsSLFWCentralSlicePerLBY = currentLumiBlock;
                  if(slices[iSlice].cand[iCand].errorFlag)
                      fill(m_packageName, candErrorFlagVsSLFWCentralSlicePerLBX, candErrorFlagVsSLFWCentralSlicePerLBY);

                  candSliceVsSLFWX = num;
                  candSliceVsSLFWY = 0;
                  fill(m_packageName, candSliceVsSLFWX, candSliceVsSLFWY);
              }
              else
              {
                  candRoiVsSLFWOtherSliceX = slices[iSlice].cand[iCand].num + 24*(1-slices[iSlice].cand[iCand].side);//offset side C
                  candRoiVsSLFWOtherSliceY = slices[iSlice].cand[iCand].roi;
                  fill(m_packageName, candRoiVsSLFWOtherSliceX, candRoiVsSLFWOtherSliceY);

                  candSliceVsSLFWX = num;
                  candSliceVsSLFWY = 1;
                  fill(m_packageName, candSliceVsSLFWX, candSliceVsSLFWY);
              }
          }

      }

      //-------------------------------------------------
      //TOBs
      //-------------------------------------------------

      ///per Side
      {
          /// 2D eta-phi hits
          /// (clarify eta bits)

          /// 2D eta/phi - fill when GoodMF    (EF only)
          /// 2D eta/phi - fill when InnerCoin (EF only)
          /// 2D eta/phi - fill when BW23      (EF only)
          /// 2D eta/phi - fill when charge    (EF only)

          /// 2D pT VS phi
          /// 2D pT VS eta
      }

      ///if tobCount!=tob words
      if(header_tobCount != slices[iSlice].tob.size())
      {
          errorSummaryMUCTPIX=3;
          fill(m_packageName, errorSummaryMUCTPIX);
          errorSummaryPerLumiBlockMUCTPIX=2;
          errorSummaryPerLumiBlockMUCTPIY=currentLumiBlock;
          fill(m_packageName, errorSummaryPerLumiBlockMUCTPIX, errorSummaryPerLumiBlockMUCTPIY);
          //ATH_MSG_WARNING( "MUCTPI DQ DEBUG: iSlice="<<iSlice << " header_tobCount="<<header_tobCount<<" slices[iSlice].tob.size()="<<slices[iSlice].tob.size() );
      }

      for(uint iTOB=0;iTOB<slices[iSlice].tob.size();iTOB++)
      {

          uint eta = slices[iSlice].tob[iTOB].etaRaw;
          uint phi = slices[iSlice].tob[iTOB].phiRaw;
          uint pt = slices[iSlice].tob[iTOB].pt;

          if(slices[iSlice].tob[iTOB].side==1)//A
          {
              tobEtaPhiAX = phi;
              tobEtaPhiAY = eta;
              fill(m_packageName, tobEtaPhiAX, tobEtaPhiAY);

              if(slices[iSlice].tob[iTOB].candFlag_GoodMF)
              {
                  tobEtaPhiA_GoodMFX = phi;
                  tobEtaPhiA_GoodMFY = eta;
                  fill(m_packageName, tobEtaPhiA_GoodMFX, tobEtaPhiA_GoodMFY);
              }
              if(slices[iSlice].tob[iTOB].candFlag_InnerCoin)
              {
                  tobEtaPhiA_InnerCoinX = phi;
                  tobEtaPhiA_InnerCoinY = eta;
                  fill(m_packageName, tobEtaPhiA_InnerCoinX, tobEtaPhiA_InnerCoinY);
              }
              if(slices[iSlice].tob[iTOB].candFlag_BW23)
              {
                  tobEtaPhiA_BW23X = phi;
                  tobEtaPhiA_BW23Y = eta;
                  fill(m_packageName, tobEtaPhiA_BW23X, tobEtaPhiA_BW23Y);
              }
              if(slices[iSlice].tob[iTOB].candFlag_Charge)
              {
                  tobEtaPhiA_ChargeX = phi;
                  tobEtaPhiA_ChargeY = eta;
                  fill(m_packageName, tobEtaPhiA_ChargeX, tobEtaPhiA_ChargeY);
              }

              tobPtVsEtaAX = eta;
              tobPtVsEtaAY = pt;
              fill(m_packageName, tobPtVsEtaAX, tobPtVsEtaAY);
              tobPtVsPhiAX = eta;
              tobPtVsPhiAY = pt;
              fill(m_packageName, tobPtVsPhiAX, tobPtVsPhiAY);
          }//side A
          else
          {
              tobEtaPhiCX = phi;
              tobEtaPhiCY = eta;
              fill(m_packageName, tobEtaPhiCX, tobEtaPhiCY);

              if(slices[iSlice].tob[iTOB].candFlag_GoodMF)
              {
                  tobEtaPhiC_GoodMFX = phi;
                  tobEtaPhiC_GoodMFY = eta;
                  fill(m_packageName, tobEtaPhiC_GoodMFX, tobEtaPhiC_GoodMFY);
              }
              if(slices[iSlice].tob[iTOB].candFlag_InnerCoin)
              {
                  tobEtaPhiC_InnerCoinX = phi;
                  tobEtaPhiC_InnerCoinY = eta;
                  fill(m_packageName, tobEtaPhiC_InnerCoinX, tobEtaPhiC_InnerCoinY);
              }
              if(slices[iSlice].tob[iTOB].candFlag_BW23)
              {
                  tobEtaPhiC_BW23X = phi;
                  tobEtaPhiC_BW23Y = eta;
                  fill(m_packageName, tobEtaPhiC_BW23X, tobEtaPhiC_BW23Y);
              }
              if(slices[iSlice].tob[iTOB].candFlag_Charge)
              {
                  tobEtaPhiC_ChargeX = phi;
                  tobEtaPhiC_ChargeY = eta;
                  fill(m_packageName, tobEtaPhiC_ChargeX, tobEtaPhiC_ChargeY);
              }

              tobPtVsEtaCX = eta;
              tobPtVsEtaCY = pt;
              fill(m_packageName, tobPtVsEtaCX, tobPtVsEtaCY);
              tobPtVsPhiCX = eta;
              tobPtVsPhiCY = pt;
              fill(m_packageName, tobPtVsPhiCX, tobPtVsPhiCY);
          }//side C

      }//TOB for loop


      //-------------------------------------------------
      //combined
      //-------------------------------------------------

      ///if cand<=32 && topo<=32, check if equal
      /// todo: maybe should move this check in the RDO building actually...?
      if(slices[iSlice].cand.size()<=32 && slices[iSlice].tob.size()<=32)
          if(slices[iSlice].cand.size()!=slices[iSlice].tob.size())
              ATH_MSG_WARNING( "MUCTPI DQ DEBUG: Cand & TOB #words not equal. LB="<<std::dec<<currentLumiBlock);


  }// slice for loop

  //-------------------------------------------------
  //status data word (last word in data words)
  //-------------------------------------------------

  /// 1D status word errors (16 bits)
  /// 2D status word errors VS LB
  /// todo: need to add 1D TProfile to make ratios?

  //get muctpi fragment data in the form of a vector of timeslices
  const std::vector< size_t > &errorBits = theMuCTPI_RDO->errorBits();

  for(uint iBit=0;iBit<errorBits.size();iBit++)
  {
      if(errorBits[iBit])
      {
          statusDataWordMUCTPIX=iBit;
          fill(m_packageName, statusDataWordMUCTPIX);
          statusDataWordPerLumiBlockMUCTPIX=iBit;
          statusDataWordPerLumiBlockMUCTPIY=currentLumiBlock;
          fill(m_packageName, statusDataWordPerLumiBlockMUCTPIX, statusDataWordPerLumiBlockMUCTPIY);
      }
  }

}

void
TrigT1CTMonitoring::BSMonitoringAlgorithm::doCtp(const CTP_RDO* theCTP_RDO,
						 const CTP_RIO* theCTP_RIO,
						 const EventContext& ctx) const
{
  ATH_MSG_DEBUG( "CTPMON begin doCtp()");
  //ERROR histos
  auto errorSummaryCTPX = Monitored::Scalar<int>("errorSummaryCTPX",0);
  auto errorSummaryCTPY = Monitored::Scalar<int>("errorSummaryCTPY",0);
  auto errorSummaryPerLumiBlockCTPX = Monitored::Scalar<int>("errorSummaryPerLumiBlockCTPX",0);
  auto errorSummaryPerLumiBlockCTPY = Monitored::Scalar<int>("errorSummaryPerLumiBlockCTPY",0);
  auto errorPerLumiBlockX = Monitored::Scalar<int>("errorPerLumiBlockX",0);
  //CTP
  auto deltaBcidX = Monitored::Scalar<int>("deltaBcidX",0);
  auto triggerTypeX = Monitored::Scalar<int>("triggerTypeX",0);
  auto timeSinceLBStartX = Monitored::Scalar<int>("timeSinceLBStartX",0);
  auto timeUntilLBEndX = Monitored::Scalar<int>("timeUntilLBEndX",0);
  auto timeSinceL1AX = Monitored::Scalar<int>("timeSinceL1AX",0);
  auto turnCounterTimeErrorX = Monitored::Scalar<int>("turnCounterTimeErrorX",0);
  auto turnCounterTimeErrorVsLbX = Monitored::Scalar<int>("turnCounterTimeErrorVsLbX",0);
  auto turnCounterTimeErrorVsLbY = Monitored::Scalar<int>("turnCounterTimeErrorVsLbY",0);
  auto pitBCX = Monitored::Scalar<int>("pitBCX",0);
  auto pitBCY = Monitored::Scalar<int>("pitBCY",0);
  auto pitFirstBCX = Monitored::Scalar<int>("pitFirstBCX",0);
  auto pitFirstBCY = Monitored::Scalar<int>("pitFirstBCY",0);
  auto tavX = Monitored::Scalar<int>("tavX",0);
  auto tavY = Monitored::Scalar<int>("tavY",0);
  auto ctpStatus1X = Monitored::Scalar<int>("ctpStatus1X",0);
  auto ctpStatus2X = Monitored::Scalar<int>("ctpStatus2X",0);

  auto eventInfo = GetEventInfo(ctx);

  //get the cool data here:
  uint64_t lb_stime = 0;
  uint64_t lb_etime = 0;
  bool retrievedLumiBlockTimes = false;
  if (!m_isSim) {
    SG::ReadCondHandle<AthenaAttributeList> lblb(m_LBLBFolderInputKey, ctx);
    const AthenaAttributeList* lblbattrList{*lblb};
    if (lblbattrList==nullptr) {
      ATH_MSG_WARNING("Failed to retrieve /TRIGGER/LUMI/LBLB " << m_LBLBFolderInputKey.key() << " not found");
    }
    else {
      retrievedLumiBlockTimes = true;
      auto lb_stime_loc = (*lblbattrList)["StartTime"].data<cool::UInt63>();
      auto lb_etime_loc = (*lblbattrList)["EndTime"].data<cool::UInt63>();
      lb_stime = lb_stime_loc;
      lb_etime = lb_etime_loc;
      ATH_MSG_DEBUG("lb_stime: " << lb_stime << " lb_etime: " << lb_etime );
    }
  }

  //currentBeamMode
  std::string currentBeamMode = "sorry: not available!";
  if (!m_isSim){
    SG::ReadCondHandle<CondAttrListCollection> fillstate(m_FILLSTATEFolderInputKey,ctx);
    const CondAttrListCollection* fillstateattrListColl { *fillstate };
    if ( fillstateattrListColl == nullptr )    {
      ATH_MSG_WARNING("/LHC/DCS/FILLSTATE " << m_FILLSTATEFolderInputKey.key() << " not found");
    }
    else{
      CondAttrListCollection::const_iterator itrcoll;
      for (itrcoll = fillstateattrListColl->begin(); itrcoll != fillstateattrListColl->end(); ++itrcoll) {
	const coral::AttributeList &atr = itrcoll->second;
	currentBeamMode = *(static_cast<const std::string *>((atr["BeamMode"]).addressOfData()));
      }
    }
  }
  ATH_MSG_DEBUG("BeamMode: " << currentBeamMode  );
  
  //DataTakingMode
  int readyForPhysics = -1;
  if (!m_isSim){
    SG::ReadCondHandle<AthenaAttributeList> datatakingmode(m_DataTakingModeFolderInputKey, ctx);
    const AthenaAttributeList* datatakingmodeattrList{*datatakingmode};
    if (datatakingmodeattrList==nullptr) {
      ATH_MSG_WARNING( "Failed to retrieve /TDAQ/RunCtrl/DataTakingMode with key " << m_DataTakingModeFolderInputKey.key());
    }
    else{
      auto readyForPhysics_loc =  (*datatakingmodeattrList)["ReadyForPhysics"].data<uint32_t>();
      readyForPhysics = readyForPhysics_loc;
      ATH_MSG_DEBUG( "readyForPhysics: " << readyForPhysics);
    }
  }
  uint32_t evId = 0;
  uint32_t headerBcid = 0;
  int ttype=0;

  CTP_Decoder ctp;
  ctp.setRDO(theCTP_RDO);

  if (theCTP_RIO) {
    evId = theCTP_RIO->getLvl1Id();
    ttype = theCTP_RIO->getLvl1TriggerType();
    triggerTypeX = ttype;
    fill(m_packageName, triggerTypeX);

    headerBcid = (theCTP_RIO->getBCID() & 0xf);

    uint32_t currentLumiBlock = eventInfo->lumiBlock();
    // check that the LB number is the same in the EventInfo and the CTP_RIO. TODO: add error for this?
    if (currentLumiBlock != (theCTP_RIO->getDetectorEventType() & 0xffff)) {
      ATH_MSG_WARNING( "LB number in EventInfo (" << currentLumiBlock 
		       << ") does not match the one in the CTP_RIO object (" 
		       << (theCTP_RIO->getDetectorEventType() & 0xffff) << ")");
    }
    ATH_MSG_DEBUG( "Run number: " << eventInfo->runNumber() << ", Event: " << eventInfo->eventNumber() << ", LB: " << eventInfo->lumiBlock() ); 

    //if (currentLumiBlock > m_maxLumiBlock) m_maxLumiBlock = currentLumiBlock;
    if (currentLumiBlock < 1 ) //|| 
      //	(retrievedLumiBlockTimes && (find(m_lumiBlocks.begin(), m_lumiBlocks.end(), currentLumiBlock) == m_lumiBlocks.end()))) {
      //patrick check this
      {
      ATH_MSG_WARNING( "Invalid lumi block: " << currentLumiBlock);
      errorSummaryCTPX = 3;
      errorSummaryCTPY = 1;
      fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
      errorSummaryPerLumiBlockCTPX = currentLumiBlock;
      errorSummaryPerLumiBlockCTPY = 3;
      fill(m_packageName, errorSummaryPerLumiBlockCTPX, errorSummaryPerLumiBlockCTPY);
      errorPerLumiBlockX = currentLumiBlock;
      fill(m_packageName, errorPerLumiBlockX);
    }
    else {
      errorSummaryCTPX = 3;
      errorSummaryCTPY = 0;
      fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
      if (retrievedLumiBlockTimes) {
	uint64_t eventTime = static_cast<uint64_t>(theCTP_RDO->getTimeSec()*1e09 + theCTP_RDO->getTimeNanoSec());
	if (eventTime < lb_stime || eventTime > lb_etime) {
	  ATH_MSG_WARNING( "Event time (" << eventTime 
			   << ") not within time interval for current lumi block (LB: " << currentLumiBlock 
			   << ", start: " <<  lb_stime 
			   << ", stop: " << lb_etime << ")");
      errorSummaryCTPX = 4;
      errorSummaryCTPY = 1;
      fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
      errorSummaryPerLumiBlockCTPX = currentLumiBlock;
      errorSummaryPerLumiBlockCTPY = 4;
      fill(m_packageName, errorSummaryPerLumiBlockCTPX, errorSummaryPerLumiBlockCTPY);
	  errorPerLumiBlockX = currentLumiBlock;
	  fill(m_packageName, errorPerLumiBlockX);
	}
	else {
          errorSummaryCTPX = 4;
          errorSummaryCTPY = 0;
          fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
	  timeSinceLBStartX = (eventTime-lb_stime)/1e06;
          fill(m_packageName, timeSinceLBStartX);
	  timeUntilLBEndX = (lb_etime-eventTime)/1e06;
	  fill(m_packageName, timeUntilLBEndX);
	}

	// turn counter monitoring - first store turn counter, bcid and times for the first processed event

	// use the best available bunch-crossing interval
	double bcDurationInNs = m_defaultBcIntervalInNs;
	double freqFromCool = -1.0; //getFrequencyMeasurement(eventTime);
	if (freqFromCool > 40.078e6 && freqFromCool < 40.079e6) { // f prop. to beta, ok from HI injection to pp @ sqrt(s) = 14 TeV
	  // use average frequency since start of LB
	  // patrick
	  double lbStartFreqFromCool = -1.0; //getFrequencyMeasurement(lb_stime);
	  //double lbStartFreqFromCool = getFrequencyMeasurement(m_lbStartTimes[currentLumiBlock]);
	  if (lbStartFreqFromCool > 40.078e6 && lbStartFreqFromCool < 40.079e6) {
	    bcDurationInNs = 2./(freqFromCool+lbStartFreqFromCool)*1e9;
	  }
	  // or simply use the measurement closest to the event time
	  else {
	    bcDurationInNs = 1./freqFromCool*1e9;
	  }
	  ATH_MSG_DEBUG( "Will use BC interval calculated from frequency measurement(s) in COOL: f = " 
			 << freqFromCool << " Hz => t_BC = " << bcDurationInNs << " ns"); 
	}
	else {
	  //ATH_MSG_WARNING( "No valid frequency measurements found in COOL, will use hardcoded BC interval: t_BC = "
 	  ATH_MSG_INFO( "No valid frequency measurements found in COOL, will use hardcoded BC interval: t_BC = " 
			 << bcDurationInNs << " ns");
	}

	uint32_t lumiBlockOfPreviousEvent = eventInfo->lumiBlock() -1 ; 
	//patrick fix this!!! - first event! 
	uint64_t firstEventTime = 0;
	int64_t firstEventBcid = 0;
	int64_t firstEventTC = 0;
	// set the reference variables for the turn counter monitoring if this is the first processed event of the run/LB
	if ( (lumiBlockOfPreviousEvent != 0 && lumiBlockOfPreviousEvent != currentLumiBlock) ) {
	  //if ( !m_eventCount || (lumiBlockOfPreviousEvent != 0 && lumiBlockOfPreviousEvent != currentLumiBlock) ) {
	  firstEventTime = eventTime;
	  firstEventBcid = static_cast<int64_t>(theCTP_RIO->getBCID());
	  firstEventTC = static_cast<int64_t>(theCTP_RDO->getTurnCounter());
	}

	// calculate the time passed since the first processed event, based on GPS clock and turn counter+bcid
	int64_t timeDiff_GPS = eventTime - firstEventTime; 
	int64_t firstEventTimeInBc_TC = firstEventTC*m_bcsPerTurn+firstEventBcid;
	int64_t eventTimeInBc_TC = static_cast<int64_t>(theCTP_RDO->getTurnCounter())*m_bcsPerTurn+theCTP_RIO->getBCID();
	int64_t timeDiffInBc_TC = eventTimeInBc_TC-firstEventTimeInBc_TC;

	// fill turn counter monitoring plots if at least one of first and current turn-counter values are non-zero
	if ( !(firstEventTC == 0 && theCTP_RDO->getTurnCounter() == 0) ) {
	  std::string bm = currentBeamMode; 
	  double tDiffInNs = timeDiffInBc_TC*bcDurationInNs-timeDiff_GPS;
	  // flag an error if the offset for the timestamp calculated from TC+BCID is off by > half an LHC turn
	  // (if we're in STABLE BEAMS and did not just transition to ATLAS_READY in this LB)
	  if ( (bm == "STABLEBEAMS" || bm  == "STABLE BEAMS") && 
	       //patrick: do we need this condition?
	       //!((m_DataTakingModeFolderInputKey.find(currentLumiBlock) != m_DataTakingModeFolderInputKey.end()) &&
		 (readyForPhysics == 1) && 
	       (std::abs(tDiffInNs) > 45000) ) { 
	    ATH_MSG_WARNING( "Turn-counter based time off by " << tDiffInNs 
			     << " ns (> 0.5 LHC turn) during stable beams - missing orbit pulse?");
        errorSummaryCTPX = 10;
        errorSummaryCTPY = 1;
        fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
        errorSummaryPerLumiBlockCTPX = currentLumiBlock;
        errorSummaryPerLumiBlockCTPY = 10;
        fill(m_packageName, errorSummaryPerLumiBlockCTPX, errorSummaryPerLumiBlockCTPY);
	    errorPerLumiBlockX = currentLumiBlock;
	    fill(m_packageName, errorPerLumiBlockX);
	  }
      else {
          errorSummaryCTPX = 10;
          errorSummaryCTPY = 0;
          fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
      }
	  turnCounterTimeErrorX = tDiffInNs/1e03;
	  fill(m_packageName, turnCounterTimeErrorX);
	  turnCounterTimeErrorVsLbX = currentLumiBlock;
	  turnCounterTimeErrorVsLbY = tDiffInNs/1e03;
	  fill(m_packageName, turnCounterTimeErrorVsLbX, turnCounterTimeErrorVsLbY);
	}
	else {
	  ATH_MSG_DEBUG( "Turn counter = 0 for both first processed and current event, not filling TC histograms");
	}
      }
    }
  }
  uint32_t currentLumiBlock = eventInfo->lumiBlock();
  if (theCTP_RDO->getTimeNanoSec() > 1e09) {
    ATH_MSG_WARNING( "Nanosecond timestamp too large: " << theCTP_RDO->getTimeNanoSec());
    errorSummaryCTPX = 5;
    errorSummaryCTPY = 1;
    fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
    errorSummaryPerLumiBlockCTPX = currentLumiBlock;
    errorSummaryPerLumiBlockCTPY = 5;
    fill(m_packageName, errorSummaryPerLumiBlockCTPX, errorSummaryPerLumiBlockCTPY);
    errorPerLumiBlockX = currentLumiBlock;
    fill(m_packageName, errorPerLumiBlockX);
  } else {
    errorSummaryCTPX = 5;
    errorSummaryCTPY = 0;
    fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
  }

  std::vector<uint32_t> vec=theCTP_RDO->getEXTRAWords();
  timeSinceL1AX = theCTP_RDO->getTimeSinceLastL1A()*25*10E-6;
  fill(m_packageName, timeSinceL1AX);

  uint32_t numberBC = theCTP_RDO->getNumberOfBunches();
  if (numberBC > 0) {
    unsigned int storeBunch = theCTP_RDO->getL1AcceptBunchPosition();
    const std::vector<CTP_BC> &BCs = ctp.getBunchCrossings();
    const CTP_BC & bunch = BCs[storeBunch];
    unsigned int bcid = bunch.getBCID();

    double bcid_offset = (static_cast < int >((bcid)&0xf) - static_cast < int >(headerBcid));
    deltaBcidX = bcid_offset;
    fill(m_packageName, deltaBcidX);
    if (bcid_offset != 0) {
      if (!m_runOnESD) {
	ATH_MSG_WARNING( "Found BCID offset of "<< bcid_offset << " between ROD Header (" 
			 << headerBcid << ") and data (" << (bcid&0xf) << ")");
    errorSummaryCTPX = 1;
    errorSummaryCTPY = 1;
    fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
    errorSummaryPerLumiBlockCTPX = currentLumiBlock;
    errorSummaryPerLumiBlockCTPY = 1;
    fill(m_packageName, errorSummaryPerLumiBlockCTPX, errorSummaryPerLumiBlockCTPY);
	errorPerLumiBlockX = currentLumiBlock;
	fill(m_packageName, errorPerLumiBlockX);
      }
    }
    else {
      errorSummaryCTPX = 1;
      errorSummaryCTPY = 0;
      fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
    }
    /*
     * TIP,TBP,TAP,TAV 
     */
    short bunchIndex = -storeBunch;
    std::bitset<512> TIPfirst;
    std::bitset<512> TBPfirst;
    std::bitset<512> TAPfirst;
    TIPfirst.set();
    TBPfirst.set();
    TAPfirst.set();

    //Vectors of TBP/TAP/TAB
    std::vector<int> tbpItems;
    std::vector<int> tapItems;
    std::vector<int> tavItems;
    std::vector<int> tbpBC;
    std::vector<int> tapBC;
    std::vector<int> tavBC;
    //int minbc=bunchIndex;
    for ( std::vector<CTP_BC>::const_iterator it = BCs.begin();
	  it != BCs.end(); ++it, ++bunchIndex ) {
      bcid = it->getBCID();

      if ( (!bunchIndex) && (m_compareRerun) ) {//gives position of L1A
        StatusCode sc = compareRerun(*it, ctx);
        if ( sc.isFailure() ) {
          ATH_MSG_WARNING( "compareRerun() returned failure");
        }
      }

      const std::bitset<512> currentTIP(it->getTIP());
      if (currentTIP.any()) {
	for ( size_t tipNum = 0; tipNum < currentTIP.size(); ++tipNum ) {
	  if (currentTIP.test(tipNum)) {
	    pitBCX = tipNum;
	    pitBCY = bunchIndex;
	    fill(m_packageName, pitBCX, pitBCY);
	    if (TIPfirst.test(tipNum)) {
	      TIPfirst.set(tipNum,0);
	      pitFirstBCX = tipNum;
	      pitFirstBCY = bunchIndex;
	      fill(m_packageName, pitFirstBCX, pitFirstBCY);
	    }
	  }
	}
      }

      const std::bitset<512> currentTBP(it->getTBP());
      if (currentTBP.any()) {
	for ( size_t item = 0; item < currentTBP.size(); ++item ) {
	  if (currentTBP.test(item)) {
	    tbpItems.push_back(item);
	    tbpBC.push_back(bunchIndex);
	    if (TBPfirst.test(item)) {
	      TBPfirst.set(item,0);
	    }
	  }
	}
      }
      
      const std::bitset<512> currentTAP(it->getTAP());
      if (currentTAP.any()) {
	for ( size_t item = 0; item < currentTAP.size(); ++item ) {
	  if (currentTAP.test(item)) {
	    tapItems.push_back(item);
	    tapBC.push_back(bunchIndex);
	    if (TAPfirst.test(item)) {
	      TAPfirst.set(item,0);
	    }
	  }
	}
      }

      const std::bitset<512> currentTAV(it->getTAV());

      if (currentTAV.any()) {
	for ( size_t item = 0; item < currentTAV.size(); ++item ) {
	  if (currentTAV.test(item)) {
	    tavX = item;
	    fill(m_packageName, tavX);
	    tavItems.push_back(item);
	    tavBC.push_back(bunchIndex);
	  }
	}
      }
    }
    //int maxbc=bunchIndex-1;

    bool allTAPFine=true;
    bool allTAVFine=true;
    for ( unsigned int i=0; i<tapItems.size(); i++ ) {
      ATH_MSG_DEBUG( tapItems.at(i) << " TAP fired at BC " << tapBC.at(i));
      bool isTBP=false;
      for ( unsigned int j=0; j<tbpItems.size() && isTBP==false; j++ ) {
	if ( tbpItems.at(j)==tapItems.at(i) && tbpBC.at(j)==tapBC.at(i) ) isTBP=true;
      }
      if ( isTBP==false ) {
	allTAPFine=false;
	ATH_MSG_WARNING( "TAP item " << tapItems.at(i) << " at BC " << tapBC.at(i) << " not found in TBP");
      }
    }
    for ( unsigned int i=0; i<tavItems.size(); i++ ) {
      ATH_MSG_DEBUG( tavItems.at(i) << " TAV fired at BC " << tavBC.at(i));
      bool isTAP=false;
      for ( unsigned int j=0; j<tapItems.size() && isTAP==false; j++ ) {
	if ( tapItems.at(j)==tavItems.at(i) && tapBC.at(j)==tavBC.at(i) ) isTAP=true;
      }
      if ( isTAP==false ) {
	allTAVFine=false;
	ATH_MSG_WARNING( "TAV item " << tavItems.at(i) << " at BC " << tavBC.at(i) << " not found in TAP");
      }
    }

    //Fill Error Hist
    if (allTAPFine==false) {
      errorSummaryCTPX = 6;
      errorSummaryCTPY = 1;
      fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
      errorSummaryPerLumiBlockCTPX = currentLumiBlock;
      errorSummaryPerLumiBlockCTPY = 6;
      fill(m_packageName, errorSummaryPerLumiBlockCTPX, errorSummaryPerLumiBlockCTPY);
      errorPerLumiBlockX = currentLumiBlock;
      fill(m_packageName, errorPerLumiBlockX);
    }
    else {
      errorSummaryCTPX = 6;
      errorSummaryCTPY = 0;
      fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
    }
    if (allTAVFine==false) {
      errorSummaryCTPX = 7;
      errorSummaryCTPY = 1;
      fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
      errorSummaryPerLumiBlockCTPX = currentLumiBlock;
      errorSummaryPerLumiBlockCTPY = 7;
      fill(m_packageName, errorSummaryPerLumiBlockCTPX, errorSummaryPerLumiBlockCTPY);
      errorPerLumiBlockX = currentLumiBlock;
      fill(m_packageName, errorPerLumiBlockX);
    }
    else {
      errorSummaryCTPX = 7;
      errorSummaryCTPY = 0;
      fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
    }
    //m_lastminbc=minbc;
    //m_lastmaxbc=maxbc;

    // TODO: is this really the check we want to do? Doesn't offline in general have more resources..? /CO
    //if (m_environment==AthenaMonManager::online)
    //  updateRangeUser();//Triggers LW->ROOT conversion so should certainly not be done offline

    errorSummaryCTPX = 2;
    errorSummaryCTPY = 0;
    fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);

    if (msgLvl(MSG::DEBUG)) {
    std::vector<unsigned int> triggersFired = ctp.getAllTriggers(storeBunch);
    std::stringstream str;
    //for ( unsigned int i = 0; i < triggersFired.size(); ++i ) {
    for (auto i : triggersFired ) { 
      str << i << " "; 
    }
    ATH_MSG_DEBUG( triggersFired.size() << " trigger items fired: " << str.str());
    }
  } 
  else {
    if (!m_runOnESD) {
      ATH_MSG_WARNING( "Zero bunches in CTP data for ext. LVL1 ID 0x" << MSG::hex << evId << MSG::dec);
      errorSummaryCTPX = 2;
      errorSummaryCTPY = 1;
      fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
      errorSummaryPerLumiBlockCTPX = currentLumiBlock;
      errorSummaryPerLumiBlockCTPY = 2;
      fill(m_packageName, errorSummaryPerLumiBlockCTPX, errorSummaryPerLumiBlockCTPY);
      errorPerLumiBlockX = currentLumiBlock;
      fill(m_packageName, errorPerLumiBlockX);
    }
  }
  
  /*
   * Check the error status words of the ROD Header 
   */
  if (theCTP_RIO) {
    uint32_t num = theCTP_RIO->getNumberStatusWords();
    std::vector<uint32_t> vStatus = theCTP_RIO->getStatusWords();
    for ( uint32_t i = 0; i < num; ++i ) {
      if (vStatus[i] != 0) {
	int Status = -1;
	if (i == 0) {
	  ATH_MSG_DEBUG( "CTP error status word #" << i << ": 0x" << MSG::hex << vStatus[i] << MSG::dec);
	  Status = 1;
	} else if (i == 1) {
	  Status = 2;
	} else {
	  continue;
	}
	for ( int bit = 0; bit < 24; ++bit ) {
	  if (vStatus[i] & (1 << bit)){
	    if (Status == 1) {
	      ctpStatus1X = bit;
              fill(m_packageName, ctpStatus1X);
	    }
            else if (Status == 2) {
	      ctpStatus2X = bit;
              fill(m_packageName, ctpStatus2X);
	    }
	  }
	}
      }
    }
  }
}


void
TrigT1CTMonitoring::BSMonitoringAlgorithm::doCtpMuctpi( const CTP_RDO* theCTP_RDO, 
							//const CTP_RIO* theCTP_RIO,
							const MuCTPI_RDO* theMuCTPI_RDO, 
							//const MuCTPI_RIO* theMuCTPI_RIO,
							const EventContext& ctx) const
{
  ATH_MSG_DEBUG( "CTPMON begin doCtpMuctpi()");

}


StatusCode
TrigT1CTMonitoring::BSMonitoringAlgorithm::compareRerun(const CTP_BC &bunchCrossing,
							const EventContext& ctx) const
{
  bool itemMismatch{false};
  //ERROR histos
  auto errorSummaryCTPX = Monitored::Scalar<int>("errorSummaryCTPX",0);
  auto errorSummaryCTPY = Monitored::Scalar<int>("errorSummaryCTPY",0);
  auto errorSummaryPerLumiBlockCTPX = Monitored::Scalar<int>("errorSummaryPerLumiBlockCTPX",0);
  auto errorSummaryPerLumiBlockCTPY = Monitored::Scalar<int>("errorSummaryPerLumiBlockCTPY",0);
  auto errorPerLumiBlockX = Monitored::Scalar<int>("errorPerLumiBlockX",0);
  //compareRerun
  auto l1ItemsBPSimMismatchX = Monitored::Scalar<int>("l1ItemsBPSimMismatchX",0);
  auto l1ItemsBPSimMismatchY = Monitored::Scalar<int>("l1ItemsBPSimMismatchY",0);
  //the following is char - correctly filled?
  auto l1ItemsBPSimMismatchItemsX = Monitored::Scalar<std::string>("l1ItemsBPSimMismatchItemsX","");
  auto l1ItemsBPSimMismatchItemsY = Monitored::Scalar<int>("l1ItemsBPSimMismatchItemsY",0);

  auto eventInfo = GetEventInfo(ctx);

  const CTP_RDO* theCTP_RDO_Rerun = nullptr;
  ATH_MSG_DEBUG( "Retrieving CTP_RDO from SG with key CTP_RDO_Rerun");
  //ATH_MSG_INFO( "Retrieving CTP_RDO from SG with key CTP_RDO_Rerun");
  CHECK( (theCTP_RDO_Rerun = SG::get(m_CTP_RDO_RerunKey, ctx)) != nullptr );
  //https://gitlab.cern.ch/atlas/athena/-/blob/master/Trigger/TrigT1/TrigT1Result/src/CTP_RDO.cxx#L19
  // as I see: https://gitlab.cern.ch/atlas/athena/-/blob/master/Trigger/TrigT1/TrigT1Result/src/CTP_RDO.cxx#L51
  // CTPVersion has not been set, no information about data format available, please fix your code
  //CTP_RDO::CTP_RDO(unsigned int ctpVersionNumber, const uint32_t nBCs, uint32_t nExtraWords)
  //const uint32_t nBCs = 0;
  //uint32_t nExtraWords = 0;
  //CHECK( (theCTP_RDO_Rerun = SG::get(m_CTP_RDO_RerunKey, ctx)) != nullptr );
  //CHECK( (theCTP_RDO_Rerun = SG::get(m_CTP_RDO_RerunKey, 4, nBCs, nExtraWords, ctx)) != nullptr );
  //ATH_MSG_WARNING( "theCTP_RDO_Rerun->getCTPVersionNumber " << theCTP_RDO_Rerun->getCTPVersionNumber());
  //CTP_RDO* theCTP_RDO_Rerun2 = (CTP_RDO*)theCTP_RDO_Rerun;
  //theCTP_RDO_Rerun2->setCTPVersionNumber(4);
  //theCTP_RDO_Rerun->setCTPVersionNumber(4);
  //ATH_MSG_WARNING( "theCTP_RDO_Rerun2->getCTPVersionNumber " << theCTP_RDO_Rerun2->getCTPVersionNumber());

  CTP_Decoder ctp_rerun;
  ctp_rerun.setRDO(theCTP_RDO_Rerun);

  const std::vector<CTP_BC> ctp_bc_rerun=ctp_rerun.getBunchCrossings();
  if (ctp_bc_rerun.size() != 1) {
    ATH_MSG_ERROR( "Rerun simulation has non unity number of bunch crossings ");
    return StatusCode::FAILURE;
  }

  ATH_MSG_DEBUG( "In compareRerun: dumping data for BC " << bunchCrossing.getBCID());
  bunchCrossing.dumpData(msg());

  ATH_MSG_DEBUG( "In compareRerun: dumping rerun data for BC 0");
  ctp_bc_rerun.at(0).dumpData(msg());
   
  ATH_MSG_DEBUG( "Comparing TBP from CTP_RDO objects with keys CTP_RDO (from data) and CTP_RDO_Rerun (from simulation)");
   
  const std::bitset<512> currentTBP(bunchCrossing.getTBP());
  const std::bitset<512> currentTBP_rerun(ctp_bc_rerun.at(0).getTBP());
   
  if ( currentTBP != currentTBP_rerun ) {
    const TrigConf::L1Menu * l1menu = nullptr;
    ATH_CHECK(detStore()->retrieve(l1menu));
    for(const TrigConf::L1Item & item : *l1menu) {

      //do not include random and non-simulated triggers in this test (can be configured)
      bool skip = item.definition().find("RNDM") != std::string::npos;
      for(const std::string & p : m_ignorePatterns) {
	if(item.name().find(p) != std::string::npos) {
	  skip = true;
	  break;
	}
      }
      if( skip ) continue;

      bool tbp       = currentTBP.test( item.ctpId() );
      bool tbp_rerun = currentTBP_rerun.test( item.ctpId() );
      if ( tbp !=  tbp_rerun) {
	ATH_MSG_WARNING( "CTPSimulation TBP / TPB_rerun mismatch!! For L1Item '" << item.name()
			 << "' (CTP ID " << item.ctpId() << "): data="
			 << (tbp?"pass":"fail") << " != simulation=" << (tbp_rerun?"pass":"fail"));
	itemMismatch=true;
	l1ItemsBPSimMismatchX = item.ctpId();
	l1ItemsBPSimMismatchY = 1;
	fill(m_packageName, l1ItemsBPSimMismatchX, l1ItemsBPSimMismatchY);
	l1ItemsBPSimMismatchItemsX = (item.name()).c_str();
	l1ItemsBPSimMismatchItemsY = 1;
	fill(m_packageName, l1ItemsBPSimMismatchItemsX, l1ItemsBPSimMismatchItemsY);
      }
    }
  }
  uint32_t currentLumiBlock = eventInfo->lumiBlock();
  if (itemMismatch) {
    ATH_MSG_WARNING( "Mismatch between CTP data and simulation in BC " << bunchCrossing.getBCID());
    errorSummaryCTPX = 8;
    errorSummaryCTPY = 1;
    fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
    errorSummaryPerLumiBlockCTPX = currentLumiBlock;
    errorSummaryPerLumiBlockCTPY = 8;
    fill(m_packageName, errorSummaryPerLumiBlockCTPX, errorSummaryPerLumiBlockCTPY);
    errorPerLumiBlockX = currentLumiBlock;
    fill(m_packageName, errorPerLumiBlockX);
  } else {
    errorSummaryCTPX = 8;
    errorSummaryCTPY = 0;
    fill(m_packageName, errorSummaryCTPX, errorSummaryCTPY);
  }
  return StatusCode::SUCCESS;
}

void
TrigT1CTMonitoring::BSMonitoringAlgorithm::dumpData( const CTP_RDO* theCTP_RDO, 
						     const MuCTPI_RDO* theMuCTPI_RDO, 
						     const EventContext& ctx) const
{

  auto eventInfo = GetEventInfo(ctx);

  if ( !msgLvl(MSG::DEBUG) )
    return;
  ATH_MSG_DEBUG( "Run number: " << eventInfo->runNumber() << ", Event: " << eventInfo->eventNumber() << ", LB: " << eventInfo->lumiBlock() ); 
  if ( m_processMuctpi ) {
    // MuCTPI Multiplicity data
    MuCTPI_MultiplicityWord_Decoder multWord(theMuCTPI_RDO->
					     candidateMultiplicity(),
					     m_inclusiveTriggerThresholds);
    ATH_MSG_DEBUG("MuCTPI_Multiplicity data :");
    multWord.dumpData();
    // MuCTPI candidate data
    MuCTPI_DataWord_Decoder dataWord(0);
    ATH_MSG_DEBUG( "MIOCT candidate data :");
    std::vector<uint32_t>::const_iterator it = theMuCTPI_RDO->dataWord().begin();
    int count = 1;
    for ( ; it != theMuCTPI_RDO->dataWord().end(); ++it ) {
      ATH_MSG_DEBUG( "Candidate " << count);
      ATH_MSG_DEBUG("datawordold: " <<*it);
      dataWord.setWord(*it);
      dataWord.dumpData();
      ++count;
    }
  }

  if ( m_processCTP ) {
    // CTP information
    CTP_Decoder ctp;
    ctp.setRDO(theCTP_RDO);
    ATH_MSG_DEBUG("CTP data from CTP_RDO:");
    ctp.dumpData();

    //Misc. information
    //if ( theCTP_RIO )
    //  theCTP_RIO->dumpData();
    //if ( theMuCTPI_RIO )
    //  theMuCTPI_RIO->dumpData();
  }

}
