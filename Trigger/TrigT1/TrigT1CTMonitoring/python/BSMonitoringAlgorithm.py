#
#  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
#
def BSMonitoringConfig(inputFlags):
    '''Function to configure LVL1 BSMonitoring algorithm in the monitoring system.'''

    # local printing
    import logging
    local_logger = logging.getLogger('AthenaMonitoringCfg')
    info = local_logger.info
    #import math #only needed for phi definition  
    # get the component factory - used for getting the algorithms
    from AthenaConfiguration.ComponentFactory import CompFactory
    from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
    result = ComponentAccumulator()

    # make the athena monitoring helper
    from AthenaMonitoring import AthMonitorCfgHelper
    helper = AthMonitorCfgHelper(inputFlags,'BSMonitoringCfg')

    # get any algorithms
    BSMonAlg = helper.addAlgorithm(CompFactory.TrigT1CTMonitoring.BSMonitoringAlgorithm,'BSMonAlg')


    # default values:
    InclusiveTriggerThresholds = True
    ProcessMuctpiData = True
    ProcessMuctpiDataRIO = True
    RunOnESD = False
    CompareRerun = False
    ProcessCTPData = True
    isSimulation = inputFlags.Input.isMC

    info('In BSMonitoringConfig SIM or not?: %s', isSimulation)
    #-----------ONLINE CODE---------------------
    if inputFlags.Common.isOnline:
        #info('In BSMonitoringConfig: isOnline')
        InclusiveTriggerThresholds = True
        ProcessMuctpiData = False #True
        ProcessMuctpiDataRIO = False #True
        RunOnESD = False
        CompareRerun = False
    #-----------OFFLINE CODE---------------------
    else:
        #info('In BSMonitoringConfig: NOT isOnline')
        ## add pre algorithms for rerunning CTP simulation  
        #if 'IS_SIMULATION' not in metadata['eventTypes']:
            #info('In BSMonitoringConfig: eventTypes: %s ', metadata['eventTypes'] )
            # Wrap everything in a sequence which will force algs to execute in order, even in MT mode
            #from AthenaCommon.AlgSequence import AthSequencer
            #CTPMonSeq=AthSequencer('CTPMonSeq')
            #from AthenaConfiguration.AllConfigFlags import ConfigFlags
            #if 'IS_SIMULATION' not in metadata['eventTypes']:
            
            #none of these 2 work - pretty unsatisfying! - to be fixed asap!!!!! 
            #if inputFlags.DQ.Environment not in ('online', 'tier0', 'tier0Raw'): #, 'tier0ESD'):
                ## Wrap everything in a sequence which will force algs to execute in order, even in MT mode
                #from AthenaCommon.AlgSequence import AthSequencer
                #CTPMonSeq=AthSequencer('CTPMonSeq')

                ##only used for compareRerun()
                ##currently not yet working due to CTPVersion not set
                #from TrigT1CTP.TrigT1CTPConfig import CTPSimulationOnData
                #CTPMonSeq += CTPSimulationOnData("CTPSimulation")
                
                #from TrigT1MuctpiPhase1.TrigT1MuctpiPhase1Config import L1MuctpiPhase1_on_Data #MUCTPI_AthAlgCfg #L1MuctpiPhase1 #L1MuctpiPhase1_on_Data
                #CTPMonSeq += L1MuctpiPhase1_on_Data("MUCTPI_AthTool") # MUCTPI_AthAlgCfg(ConfigFlags) #L1MuctpiPhase1() #L1MuctpiPhase1_on_Data()
                
        # check if global muons are on 
        if not inputFlags.Reco.EnableCombinedMuon:
            if isSimulation:
                info('In BSMonitoringConfig: rec.doMuon=True & SIM')
                InclusiveTriggerThresholds = False
                ProcessMuctpiData = False
                ProcessMuctpiDataRIO = False
                CompareRerun = False #True
            else:
                info('In BSMonitoringConfig: rec.doMuon=True & DATA')
                InclusiveTriggerThresholds = False
                ProcessMuctpiData = False
                ProcessMuctpiDataRIO = False
                RunOnESD = False
                CompareRerun = False
        else:
            if isSimulation:
                info('In BSMonitoringConfig: rec.doMuon=False & SIM')
                ProcessMuctpiData = False #True
                ProcessMuctpiDataRIO = False #True
                RunOnESD = True
                CompareRerun = False #True
            else:
                info('In BSMonitoringConfig: rec.doMuon=False & DATA')
                ProcessMuctpiData = True #True
                ProcessMuctpiDataRIO = False
                RunOnESD = False #True
                CompareRerun = False

    BSMonAlg.isSimulation = isSimulation
    BSMonAlg.InclusiveTriggerThresholds = InclusiveTriggerThresholds
    BSMonAlg.ProcessMuctpiData = ProcessMuctpiData
    BSMonAlg.ProcessMuctpiDataRIO = ProcessMuctpiDataRIO
    BSMonAlg.RunOnESD = RunOnESD
    BSMonAlg.CompareRerun = CompareRerun
    BSMonAlg.ProcessCTPData = ProcessCTPData

    DefaultBcIntervalInNs = 24.9507401
    BSMonAlg.DefaultBcIntervalInNs = DefaultBcIntervalInNs
    BCsPerTurn = 3564
    BSMonAlg.BCsPerTurn = BCsPerTurn
    LumiBlockTimeCoolFolderName = '/TRIGGER/LUMI/LBLB"'
    BSMonAlg.LumiBlockTimeCoolFolderName = LumiBlockTimeCoolFolderName
    FillStateCoolFolderName = '/LHC/DCS/FILLSTATE'
    BSMonAlg.FillStateCoolFolderName = FillStateCoolFolderName
    DataTakingModeCoolFolderName = '/TDAQ/RunCtrl/DataTakingMode'
    BSMonAlg.DataTakingModeCoolFolderName = DataTakingModeCoolFolderName
    IgnorePatterns = ["L1_TRT", "L1_ZB", "_AFP", "L1_BPTX", "L1_BCM", "L1_LUCID"]
    BSMonAlg.IgnorePatterns = IgnorePatterns 

    # add monitoring algorithm to group, with group name and main directory 
    groupName = 'CTPMonitor' # the monitoring group name is also used for the package name
    BSMonAlg.PackageName = groupName
    mainDir = 'CT'
    myGroup = helper.addGroup(BSMonAlg, groupName , mainDir)

    errorSummaryCTPBinLabels = [   # .cxx indices
        "CTP/ROD BCID Offset",     #1
        "No BCs in Readout",       #2
        "Invalid Lumi Block",      #3
        "LB Out of Time",          #4
        "Nanosec > 1e9",           #5
        "TAP w/out TBP",           #6
        "TAV w/out TAP",           #7
        "CTP sim. mismatch",       #8
        "Incomplete fragment",     #9
        "Missing orbit pulse"      #10
    ]
    incompleteFragmentTypeBinLabels = [
        "CTP RIO",
        "CTP RDO",
        #"MuCTPI RIO",
        "MuCTPI RDO",
        "RoIBResult",
        "TGC SL RDO",
        "RPC SL RDO"
    ]

    # ERRORS - COMMON

    # 1d
    # CHECK( registerTH1("errorPerLumiBlock", "Number of errors per lumi block; LB number; Errors", 2001, -0.5, 2000.5) );
    myGroup.defineHistogram('errorPerLumiBlockCTPX;errorPerLumiBlock', title='Number of errors per lumi block; LB number; Errors',
                            path='',xbins=2001,xmin=-0.5,xmax=2000.5,opt='kAlwaysCreate')

    # -------------------- CTP

    monPath="/CTP/"
    myGroup.defineHistogram('errorSummaryCTPX,errorSummaryCTPY;errorSummaryCTP',title='CTP errors; ; Error ratio', type='TProfile', path=monPath, xbins=len(errorSummaryCTPBinLabels), xmin=0.5, xmax=len(errorSummaryCTPBinLabels)+0.5, ymin=-1., ymax=2., xlabels=errorSummaryCTPBinLabels, opt='kAlwaysCreate')
    myGroup.defineHistogram('errorSummaryPerLumiBlockCTPX,errorSummaryPerLumiBlockCTPY;errorSummaryPerLumiBlockCTP',title='Errors per lumi block; LB number; Errors', type='TH2F', path=monPath, xbins=2000, xmin=0.5, xmax=2000.5, ybins=len(errorSummaryCTPBinLabels), ymin=0.5, ymax=len(errorSummaryCTPBinLabels)+0.5, ylabels=errorSummaryCTPBinLabels, opt='kAlwaysCreate')
    myGroup.defineHistogram('incompleteFragmentTypeX;incompleteFragmentType', title='Number of missing fragments per type; Fragment type; Number of incomplete fragments ', path=monPath, xbins=7, xmin=-0.5, xmax=7.5, xlabels=incompleteFragmentTypeBinLabels, opt='kAlwaysCreate')
    myGroup.defineHistogram('deltaBcidX;deltaBcid', title='CTP Data BCID - ROD Header BCID; #DeltaBCID; Entries',
                            path=monPath,xbins=401,xmin=-200.5,xmax=200.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('triggerTypeX;triggerType', title='Trigger Type; Trigger Type; Entries',
                            path=monPath,xbins=256,xmin=-0.5,xmax=255.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('timeSinceLBStartX;timeSinceLBStart', title='Time Since LB Start; Time After New LB (ms); Entries',
                            path=monPath,xbins=1000,xmin=-500,xmax=1500,opt='kAlwaysCreate')
    myGroup.defineHistogram('timeUntilLBEndX;timeUntilLBEnd', title='Time Until LB End; Time Until Next LB (ms); Entries',
                            path=monPath,xbins=1000,xmin=-500,xmax=1500,opt='kAlwaysCreate')
    myGroup.defineHistogram('timeSinceL1AX;timeSinceL1A', title='Time since last L1A; Time since last L1A (ms); Entries',
                            path=monPath,xbins=2000,xmin=-1,xmax=30,opt='kAlwaysCreate')
    myGroup.defineHistogram('turnCounterTimeErrorX;turnCounterTimeError', title='Error of time based on turn counter and BCID; t_{TC+BCID}-t_{GPS} [#mus]; Entries',
                            path=monPath,xbins=2000,xmin=-1000.,xmax=1000.,opt='kAlwaysCreate')
    #patrick check how this one now looks and used to look! - compare to  error Profile!
    myGroup.defineHistogram('turnCounterTimeErrorVsLbX,turnCounterTimeErrorVsLbY,turnCounterTimeErrorVsLbZ;turnCounterTimeErrorVsLb',
                            title='Error of (TC+BCID)-based time vs. LB; LB; t_{TC+BCID}-t_{GPS} [#mus]',
                            type='TProfile2D', path=monPath, xbins=2001, ybins=2001, xmin=-0.5, xmax=2000.5, ymin=-1000., ymax=1000, opt='kAlwaysCreate')
    myGroup.defineHistogram('pitBCX,pitBCY;pitBC',title='CTP BC vs. PIT; PIT; BC',
                            type='TH2F',path=monPath, xbins=320,xmin=-0.5,xmax=319.5,ybins=127,ymin=-63.5,ymax=63.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('pitFirstBCX,pitFirstBCY;pitFirstBC',title='First CTP BC vs. PIT; PIT; BC',
                            type='TH2F',path=monPath, xbins=320,xmin=-0.5,xmax=319.5,ybins=127,ymin=-63.5,ymax=63.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('tavX;tav', title='Trigger Items After Veto; CTP TAV; Entries',
                            path=monPath,xbins=512,xmin=-0.5,xmax=511.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('ctpStatus1X;ctpStatus1', title='CTP Status Word 1; Bit; Number of times ON',
                            path=monPath,xbins=24,xmin=-0.5,xmax=23.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('ctpStatus2X;ctpStatus2', title='CTP Status Word 2; Bit; Number of times ON',
                            path=monPath,xbins=24,xmin=-0.5,xmax=23.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('l1ItemsBPSimMismatchX;l1ItemsBPSimMismatch', title='Sim mismatch L1 Items before prescale',
                            path=monPath,xbins=512,xmin=0,xmax=512,opt='kAlwaysCreate')
    myGroup.defineHistogram('l1ItemsBPSimMismatchItemsX;l1ItemsBPSimMismatchItems', title='Sim mismatch L1 Items before prescale, mismatched ones only', path=monPath,xbins=512,xmin=0,xmax=512,opt='kAlwaysCreate')

    # -------------------- MUCTPI

    errorSummaryMUCTPIBinLabels = [        #cxx indices
        "CTP/MuCTPI BCID Offset",          #1
        "Wrong Cand Word Number",          #2
        "Wrong TOB Word Number",           #3
        "Incomplete fragment",             #4
        "MuCTPI/noRPC candidate mismatch", #5
        "MuCTPI/noTGC candidate mismatch", #6
        "RPC/noMuCTPI candidate mismatch", #7
        "GC/noMuCTPI candidate mismatch"   #8
        ]

    statusDataWordMUCTPIBinLabels = [                #cxx indices - ROD format indices
        "Event# mismatch (MSPA vs TRP) (central)", #1            #0
        "Event# mismatch (MSPC vs TRP) (central)", #2            #1
        "Event# mismatch (MSPA vs MSPC)"         , #3            #2
        "BCID mismatch (TRP vs MSPA) (central)"  , #4            #3
        "BCID mismatch (TRP vs MSPC) (central)"  , #5            #4
        "BCID mismatch (MSPA vs MSPC)"           , #6            #5
        "MSPA multiplicity LVDS link CRC error"  , #7            #6
        "MSPC multiplicity LVDS link CRC error"  , #8            #7
        "SL error flag on any MSPA sector"       , #9            #8
        "SL error flag on any MSPC sector"       , #10           #9
        "Error flag in muon cand after ZS"       , #11           #10
        "CRC error on MSPA DAQ link"             , #12           #11
        "CRC error on MSPC DAQ link"             , #13           #12
        "TriggerType reception timeout error"    , #14           #13
        "MSPA DAQ link input FIFO full flag"     , #15           #14
        "MSPC DAQ link input FIFO full flag"       #16           #15
        ]

    monPath="/MUCTPI/"
    #general errors, defined in this code
    myGroup.defineHistogram('errorSummaryMUCTPIX,errorSummaryMUCTPIY;errorSummaryMUCTPI',title='MUCTPI errors; ; Error ratio', type='TProfile', path=monPath, xbins=len(errorSummaryMUCTPIBinLabels), xmin=0.5, xmax=len(errorSummaryMUCTPIBinLabels)+0.5, ymin=-1., ymax=2., xlabels=errorSummaryMUCTPIBinLabels, opt='kAlwaysCreate')
    myGroup.defineHistogram('errorSummaryPerLumiBlockMUCTPIX,errorSummaryPerLumiBlockMUCTPIY;errorSummaryPerLumiBlockMUCTPI',title='Errors per lumi block; LB number; Errors', type='TH2F', path=monPath, xbins=2000, xmin=0.5, xmax=2000.5, ybins=len(errorSummaryMUCTPIBinLabels), ymin=0.5, ymax=len(errorSummaryMUCTPIBinLabels)+0.5, ylabels=errorSummaryMUCTPIBinLabels, opt='kAlwaysCreate')
    #error bits defined in the last word of the ROD  fragment
    myGroup.defineHistogram('statusDataWordMUCTPIX,statusDataWordMUCTPIY;statusDataWordMUCTPI',title='Status word bits; ; Error ratio', type='TProfile', path=monPath, xbins=len(statusDataWordMUCTPIBinLabels), xmin=0.5, xmax=len(statusDataWordMUCTPIBinLabels)+0.5, ymin=-1., ymax=2., xlabels=statusDataWordMUCTPIBinLabels, opt='kAlwaysCreate')
    myGroup.defineHistogram('statusDataWordPerLumiBlockMUCTPIX,statusDataWordPerLumiBlockMUCTPIY;statusDataWordPerLumiBlockMUCTPI',title='Status word bits per lumi block; LB number; Errors', type='TH2F', path=monPath, xbins=2000, xmin=0.5, xmax=2000.5, ybins=len(statusDataWordMUCTPIBinLabels), ymin=0.5, ymax=len(statusDataWordMUCTPIBinLabels)+0.5, ylabels=statusDataWordMUCTPIBinLabels, opt='kAlwaysCreate')

    #mult
    monPath="/MUCTPI/Mult"
    myGroup.defineHistogram('multX;mult', title='MLT thresholds total count', path=monPath,xbins=32,xmin=-0.5,xmax=31.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('multPerLBX;multPerLBY;multPerLB', title='MLT thresholds total count - per LB', path=monPath,xbins=2000,xmin=0,xmax=2000,ybins=32,ymin=-0.5,ymax=31.5,opt='kAlwaysCreate')

    multSliceVsMultMUCTPIBinLabels = [ #cxx indices
        "Central Slice"         ,      #1
        "Other Slice"           ,      #2
    ]
    myGroup.defineHistogram('multSliceVsMultX,multSliceVsMulY;multSliceVsMult',title='Mult slice vs Thr', type='TH2F', path=monPath, xbins=32,xmin=-0.5,xmax=31.5, ybins=len(multSliceVsMultMUCTPIBinLabels), ymin=0., ymax=len(multSliceVsMultMUCTPIBinLabels), ylabels=multSliceVsMultMUCTPIBinLabels, opt='kAlwaysCreate')

    #cand
    monPath="/MUCTPI/Cand"
    myGroup.defineHistogram('candPtBAX;candPtBA', title='BA cand pT', path=monPath,xbins=6,xmin=0.5,xmax=6.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('candPtECX;candPtEC', title='EC cand pT', path=monPath,xbins=15,xmin=0.5,xmax=15.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('candPtFWX;candPtFW', title='FW cand pT', path=monPath,xbins=15,xmin=0.5,xmax=15.5,opt='kAlwaysCreate')

    myGroup.defineHistogram('candRoiVsSLBACentralSliceX,candRoiVsSLBACentralSliceY;candRoiVsSLBACentralSlice', title='BA cand RoI vs SL (central slice)', type='TH2F', path=monPath,xbins=64,xmin=-0.5,xmax=63.5,ybins=30,ymin=-0.5,ymax=29.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('candRoiVsSLECCentralSliceX,candRoiVsSLCentralSliceECY;candRoiVsSLECCentralSlice', title='EC cand RoI vs SL (central slice)', type='TH2F', path=monPath,xbins=96,xmin=-0.5,xmax=95.5,ybins=64,ymin=-0.5,ymax=63.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('candRoiVsSLFWCentralSliceX,candRoiVsSLFWCentralSliceY;candRoiVsSLFWCentralSlice', title='FW cand RoI vs SL (central slice)', type='TH2F', path=monPath,xbins=48,xmin=-0.5,xmax=47.5,ybins=148,ymin=-0.5,ymax=147.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('candRoiVsSLBAOtherSliceX,candRoiVsSLBAOtherSliceY;candRoiVsSLBAOtherSlice', title='BA cand RoI vs SL (other slice)', type='TH2F', path=monPath,xbins=64,xmin=-0.5,xmax=63.5,ybins=30,ymin=-0.5,ymax=29.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('candRoiVsSLECOtherSliceX,candRoiVsSLECOtherSliceY;candRoiVsSLECOtherSlice', title='EC cand RoI vs SL (other slice)', type='TH2F', path=monPath,xbins=96,xmin=-0.5,xmax=95.5,ybins=64,ymin=-0.5,ymax=63.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('candRoiVsSLFWOtherSliceX,candRoiVsSLFWOtherSliceY;candRoiVsSLFWOtherSlice', title='FW cand RoI vs SL (other slice)', type='TH2F', path=monPath,xbins=48,xmin=-0.5,xmax=47.5,ybins=48,ymin=-0.5,ymax=147.5,opt='kAlwaysCreate')

    candFlagsMUCTPIBinLabels_BA = [        #cxx indices
        "> 1 cand. in the RoI"  ,          #1
        "phi overlap"           ,          #2
    ]
    candFlagsMUCTPIBinLabels_ECFW = [      #cxx indices
        "charge"                ,          #1
        "BW2/3"                 ,          #2
        "InnerCoin"             ,          #2
        "GoodMF"                ,          #2
    ]
    myGroup.defineHistogram('candCandFlagsVsSLBACentralSliceX,candCandFlagsVsSLBACentralSliceY;candCandFlagssVsSLBACentralSlice', title='BA cand CandFlags vs SL (central slice)', type='TH2F', path=monPath, xbins=64, xmin=-0.5,xmax=63.5,  ybins=2, ymin=-0.5,ymax=1.5,ylabels=candFlagsMUCTPIBinLabels_BA,opt='kAlwaysCreate')
    myGroup.defineHistogram('candCandFlagssVsSLECCentralSliceX,candCandFlagssVsSLCentralSliceECY;candCandFlagsVsSLECCentralSlice', title='EC cand CandFlags vs SL (central slice)', type='TH2F', path=monPath,xbins=96, xmin=-0.5,xmax=95.5,  ybins=4, ymin=-0.5,ymax=3.5,ylabels=candFlagsMUCTPIBinLabels_ECFW,opt='kAlwaysCreate')
    myGroup.defineHistogram('candCandFlagsVsSLFWCentralSliceX,candCandFlagsVsSLFWCentralSliceY;candCandFlagsVsSLFWCentralSlice', title='FW cand CandFlags vs SL (central slice)', type='TH2F', path=monPath,  xbins=48,xmin=-0.5,xmax=47.5, ybins=4, ymin=-0.5,ymax=3.5,ylabels=candFlagsMUCTPIBinLabels_ECFW,opt='kAlwaysCreate')

    #sec err per LB
    myGroup.defineHistogram('candErrorflagVsSLBAOtherSlicePerLBX,candErrorflagVsSLBAOtherSlicePerLBY;candErrorflagVsSLBAOtherSlicePerLB', title='SL (BA) cand ErrorFlag vs LB (central slice)', type='TH2F', path=monPath,xbins=2000,xmin=0.5,xmax=2000.5,ybins=64,ymin=-0.5,ymax=63.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('candErrorflagVsSLECOtherSlicePerLBX,candErrorflagVsSLECOtherSlicePerLBY;candErrorflagVsSLECOtherSlicePerLB', title='SL (EC) cand ErrorFlag vs LB (central slice)', type='TH2F', path=monPath,xbins=2000,xmin=0.5,xmax=2000.5,ybins=96,ymin=-0.5,ymax=95.5,opt='kAlwaysCreate')
    myGroup.defineHistogram('candErrorflagVsSLFWOtherSlicePerLBX,candErrorflagVsSLFWOtherSlicePerLBY;candErrorflagVsSLFWOtherSlicePerLB', title='SL (FW) cand ErrorFlag vs LB (central slice)', type='TH2F', path=monPath,xbins=2000,xmin=0.5,xmax=2000.5,ybins=48,ymin=-0.5,ymax=47.5,opt='kAlwaysCreate')

    monPath="/MUCTPI/TOB"
    etaBins=50
    etaMin=0
    etaMax=80
    phiBins=50
    phiMin=0
    phiMax=80
    myGroup.defineHistogram('tobEtaPhiAX,tobEtaPhiAY;tobEtaPhiA',title='TOB hitmap (eta,phi)', type='TH2F', path=monPath, xbins=phiBins, xmin=phiMin, xmax=phiMax, ybins=etaBins, ymin=etaMin, ymax=etaMax, opt='kAlwaysCreate')
    myGroup.defineHistogram('tobEtaPhiA_GoodMFX,tobEtaPhiA_GoodMFY;tobEtaPhiA_GoodMF',title='TOB GoodMF flag hitmap (eta,phi)', type='TH2F', path=monPath, xbins=phiBins, xmin=phiMin, xmax=phiMax, ybins=etaBins, ymin=etaMin, ymax=etaMax, opt='kAlwaysCreate')
    myGroup.defineHistogram('tobEtaPhiA_InnerCoinX,tobEtaPhiA_InnerCoinY;tobEtaPhiA_InnerCoin',title='TOB InnerCoin flag hitmap (eta,phi)', type='TH2F', path=monPath, xbins=phiBins, xmin=phiMin, xmax=phiMax, ybins=etaBins, ymin=etaMin, ymax=etaMax, opt='kAlwaysCreate')
    myGroup.defineHistogram('tobEtaPhiA_BW23X,tobEtaPhiA_BW23Y;tobEtaPhiA_BW23',title='TOB BW23 flag hitmap (eta,phi)', type='TH2F', path=monPath, xbins=phiBins, xmin=phiMin, xmax=phiMax, ybins=etaBins, ymin=etaMin, ymax=etaMax, opt='kAlwaysCreate')
    myGroup.defineHistogram('tobEtaPhiA_ChargeX,tobEtaPhiA_ChargeY;tobEtaPhiA_Charge',title='TOB Charge flag hitmap (eta,phi)', type='TH2F', path=monPath, xbins=phiBins, xmin=phiMin, xmax=phiMax, ybins=etaBins, ymin=etaMin, ymax=etaMax, opt='kAlwaysCreate')
    myGroup.defineHistogram('tobEtaPhiCX,tobEtaPhiCY;tobEtaPhiC',title='TOB hitmap (eta,phi)', type='TH2F', path=monPath, xbins=phiBins, xmin=phiMin, xmax=phiMax, ybins=etaBins, ymin=etaMin, ymax=etaMax, opt='kAlwaysCreate')
    myGroup.defineHistogram('tobEtaPhiC_GoodMFX,tobEtaPhiC_GoodMFY;tobEtaPhiC_GoodMF',title='TOB GoodMF flag hitmap (eta,phi)', type='TH2F', path=monPath, xbins=phiBins, xmin=phiMin, xmax=phiMax, ybins=etaBins, ymin=etaMin, ymax=etaMax, opt='kAlwaysCreate')
    myGroup.defineHistogram('tobEtaPhiC_InnerCoinX,tobEtaPhiC_InnerCoinY;tobEtaPhiC_InnerCoin',title='TOB InnerCoin flag hitmap (eta,phi)', type='TH2F', path=monPath, xbins=phiBins, xmin=phiMin, xmax=phiMax, ybins=etaBins, ymin=etaMin, ymax=etaMax, opt='kAlwaysCreate')
    myGroup.defineHistogram('tobEtaPhiC_BW23X,tobEtaPhiC_BW23Y;tobEtaPhiC_BW23',title='TOB BW23 flag hitmap (eta,phi)', type='TH2F', path=monPath, xbins=phiBins, xmin=phiMin, xmax=phiMax, ybins=etaBins, ymin=etaMin, ymax=etaMax, opt='kAlwaysCreate')
    myGroup.defineHistogram('tobEtaPhiC_ChargeX,tobEtaPhiC_ChargeY;tobEtaPhiC_Charge',title='TOB Charge flag hitmap (eta,phi)', type='TH2F', path=monPath, xbins=phiBins, xmin=phiMin, xmax=phiMax, ybins=etaBins, ymin=etaMin, ymax=etaMax, opt='kAlwaysCreate')
    #pt vs eta/phi
    myGroup.defineHistogram('tobPtVsEtaAX,tobPtVsEtaAY;tobPtVsEtaA',title='TOB pT VS eta (A)', type='TH2F', path=monPath, xbins=etaBins, xmin=etaMin, xmax=etaMax, ybins=15, ymin=0.5, ymax=15.5, opt='kAlwaysCreate')
    myGroup.defineHistogram('tobPtVsPhiAX,tobPtVsPhiAY;tobPtVsPhiA',title='TOB pT VS phi (A)', type='TH2F', path=monPath, xbins=phiBins, xmin=phiMin, xmax=phiMax, ybins=15, ymin=0.5, ymax=15.5, opt='kAlwaysCreate')
    myGroup.defineHistogram('tobPtVsEtaCX,tobPtVsEtaCY;tobPtVsEtaC',title='TOB pT VS eta (C)', type='TH2F', path=monPath, xbins=etaBins, xmin=etaMin, xmax=etaMax, ybins=15, ymin=0.5, ymax=15.5, opt='kAlwaysCreate')
    myGroup.defineHistogram('tobPtVsPhiCX,tobPtVsPhiCY;tobPtVsPhiC',title='TOB pT VS phi (C)', type='TH2F', path=monPath, xbins=phiBins, xmin=phiMin, xmax=phiMax, ybins=15, ymin=0.5, ymax=15.5, opt='kAlwaysCreate')

    monPath="/MUCTPI/Timing"

    candSliceVsSLMUCTPIBinLabels = [ #cxx indices
        "Central Slice"         ,    #1
        "Other Slice"           ,    #2
    ]
    myGroup.defineHistogram('candSliceVsSLBAX,candSliceVsSLBAY;candSliceVsSLBA',title='SL (BA) slice vs SL', type='TH2F', path=monPath, xbins=64, xmin=-0.5, xmax=63.5, ybins=len(candSliceVsSLMUCTPIBinLabels), ymin=0., ymax=2., ylabels=candSliceVsSLMUCTPIBinLabels, opt='kAlwaysCreate')
    myGroup.defineHistogram('candSliceVsSLECX,candSliceVsSLECY;candSliceVsSLEC',title='SL (EC) slice vs SL', type='TH2F', path=monPath, xbins=96, xmin=-0.5, xmax=95.5, ybins=len(candSliceVsSLMUCTPIBinLabels), ymin=0., ymax=2., ylabels=candSliceVsSLMUCTPIBinLabels, opt='kAlwaysCreate')
    myGroup.defineHistogram('candSliceVsSLFWX,candSliceVsSLFWY;candSliceVsSLFW',title='SL (FW) slice vs SL', type='TH2F', path=monPath, xbins=48, xmin=-0.5, xmax=47.5, ybins=len(candSliceVsSLMUCTPIBinLabels), ymin=0., ymax=2., ylabels=candSliceVsSLMUCTPIBinLabels, opt='kAlwaysCreate')



    acc = helper.result()
    result.merge(acc)
    return result
    

if __name__=='__main__':

    # set input file and config options
    from AthenaConfiguration.AllConfigFlags import ConfigFlags
    import glob
    inputs = glob.glob('/eos/atlas/atlastier0/rucio/data22_13p6TeV/express_express/00430702/data22_13p6TeV.00430702.express_express.merge.RAW/data22_13p6TeV.00430702.express_express.merge.RAW._lb0100._SFO-ALL._0001.1')

    #config flags defined in
    #https://gitlab.cern.ch/atlas/athena/-/blob/master/Control/AthenaConfiguration/python/AllConfigFlags.py

    ConfigFlags.Input.Files = inputs
    ConfigFlags.Output.HISTFileName = 'ExampleMonitorOutput_CTPMonitoring.root'
    ConfigFlags.lock()
    #ConfigFlags.dump() # print all the configs

    from AthenaCommon.AppMgr import ServiceMgr
    ServiceMgr.Dump = False

    from AthenaConfiguration.MainServicesConfig import MainServicesCfg  
    from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
    cfg = MainServicesCfg(ConfigFlags)
    cfg.merge(PoolReadCfg(ConfigFlags))

    BSMonitorCfg = BSMonitoringConfig(ConfigFlags)
    cfg.merge(BSMonitorCfg)
    # message level for algorithm
    BSMonitorCfg.getEventAlgo('BSMonAlg').OutputLevel = 1 # 1/2 INFO/DEBUG
    # options - print all details of algorithms, very short summary 
    cfg.printConfig(withDetails=False, summariseProps = True)
    nevents=-1
    cfg.run(nevents)
